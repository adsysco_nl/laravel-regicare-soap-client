# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/adsysco/laravel-regicare-soap-client.svg?style=flat-square)](https://packagist.org/packages/adsysco/laravel-regicare-soap-client)
[![Build Status](https://img.shields.io/travis/adsysco/laravel-regicare-soap-client/master.svg?style=flat-square)](https://travis-ci.org/adsysco/laravel-regicare-soap-client)
[![Quality Score](https://img.shields.io/scrutinizer/g/adsysco/laravel-regicare-soap-client.svg?style=flat-square)](https://scrutinizer-ci.com/g/adsysco/laravel-regicare-soap-client)
[![Total Downloads](https://img.shields.io/packagist/dt/adsysco/laravel-regicare-soap-client.svg?style=flat-square)](https://packagist.org/packages/adsysco/laravel-regicare-soap-client)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require adsysco/laravel-regicare-soap-client
```

## Usage

``` php
// Usage description here
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email ta@adsysco.nl instead of using the issue tracker.

## Credits

- [Thijs van den Anker](https://github.com/adsysco)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).