<?php

if (!function_exists('parse_2d_env_array')) {
    /**
     * Parse two-dimensional array from env-variable (comma separated, then semicolon-separated)
     *
     * @param string $envVariable
     * @param string $default
     * @return array
     */
    function parse_2d_env_array($envVariable, $default = null)
    {
        return array_map(
            function ($value) {
                return explode(';', $value);
            },
            explode(',', env($envVariable, $default))
        );
    }
}
/*
 * You can place your custom package configuration in here.
 */
return [
    'wsdls' => explode(',', env('REGIAPI_WSDL', dirname(__FILE__) . DIRECTORY_SEPARATOR . '../tests/helpers/regiweb.wsdl')),
    'apiKeys' => explode(',', env('REGIAPI_APIKEY')),
    'websites' => explode(',', env('REGIAPI_WEBSITEURLS')),
    'werksoordtIdsVacaturebank' => explode(',', env('REGIAPI_WERKSOORTIDVACATUREBANK', 1)),
    'werksoortIdPrikbord' => env('REGIAPI_WERKSOORTIDPRIKBORD', null),
    'werksoortIdPraktischeHulp' => env('REGIAPI_WERKSOORTIDPRAKTISCHEHULP', null),
    'vacature_filters' => array_map(
        function ($dienst, $regio, $categorie) {
            return [
                'dienst' => ($dienst != [''] ? $dienst : []) ?: [],
                'regio' => ($regio != [''] ? $regio : []) ?: [],
                'categorie' => ($categorie != [''] ? $categorie : []) ?: [],
            ];
        },
        parse_2d_env_array('FILTER_VACATURES_DIENST', null),
        parse_2d_env_array('FILTER_VACATURES_REGIO', null),
        parse_2d_env_array('FILTER_VACATURES_CATEGORIE', null)
    ),
];

