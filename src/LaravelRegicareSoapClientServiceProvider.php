<?php

namespace Adsysco\LaravelRegicareSoapClient;

use Adsysco\LaravelRegicareSoapClient\Providers\EventServiceProvider;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\ApiSoapClient;
use Adsysco\LaravelRegicareSoapClient\Repositories\SoapRepository;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\CachedSoapClient;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\LoggingSoapClient;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Illuminate\Support\ServiceProvider;
use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\SoapClient;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\FakeSoapClientWrapper;

class LaravelRegicareSoapClientServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(EventServiceProvider::class);
        $this->mergeConfigFrom(__DIR__ . '/../config/regicare-soap.php', 'regicare-soap');
        $this->app->singleton(SoapClient::class, function ($app) {
            return $this->getCachedSoapClient($app);
        });

        $this->getAllSoapClients();
    }

    private function getAllSoapClients()
    {
        $this->app->singleton(SoapRepository::class, function ($app) {
            return new SoapRepository($app);
        });
    }

    private function getBaseSoapClient($app)
    {
        if ($app->environment('testing')) {
//            return $app->make(FakeSoapClientWrapper::class);
        }

        $apiKeys = config('regicare-soap.apiKeys');
        $wsdls = config('regicare-soap.wsdls');
        $websites = config('regicare-soap.websites');
        $werksoortIds = config('regicare-soap.werksoordtIdsVacaturebank');
        $vacatureFilters = config('client.vacature_filters');
        $wrapper = $app->make(SoapWrapper::class);
        return new ApiSoapClient(
            $apiKeys[0],
            $wsdls[0],
            false,
            '',
            $websites[0],
            $werksoortIds[0],
            $vacatureFilters[0] ?? [],
            $wrapper
        );
    }

    private function getCachedSoapClient($app)
    {
        return new CachedSoapClient($this->getLoggingSoapClient($app));
    }

    private function getLoggingSoapClient($app)
    {
        if (!config('app.debug', false) || $app->environment('testing')) {
            return $this->getBaseSoapClient($app);
        }
        return new LoggingSoapClient($this->getBaseSoapClient($app));
    }
}
