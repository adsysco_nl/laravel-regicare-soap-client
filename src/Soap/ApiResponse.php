<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap;

use Adsysco\LaravelRegicareSoapClient\Exceptions\ApiException;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;

class ApiResponse
{
    private $data;

    private $lookup = [
        SoapErrors::LOGIN_KEY_ONBEKEND => AuthenticationException::class,
        SoapErrors::LOGIN_KEY_ONBEVOEGD => AuthorizationException::class,
        SoapErrors::GEBRUIKERSACCOUNT_ONBEKEND => AuthenticationException::class,
    ];

    public function __construct($soapResult)
    {
        $this->data = collect($soapResult);
    }

    public function getData()
    {
        $this->checkForErrors();
        return $this->data->toArray();
    }

    private function checkForErrors()
    {
        if (!$this->data->has('error')) {
            return;
        }

        if (config('app.debug')) {
            Log::error('Er is iets fout gegaan', [
                'exception' => $this->data->toArray(),
                'stacktrace' => debug_backtrace(null, 10)
            ]);
        }
        $class = $this->getExceptionForError();
        throw new $class($this->data['description']);
    }

    private function getExceptionForError()
    {
        return $this->lookup[$this->data['error']] ?? ApiException::class;
    }
}
