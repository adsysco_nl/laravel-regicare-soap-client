<?php


namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts;

interface CasefileInterface
{
    /**
     * @return mixed
     */
    public function getCasefiles();

    /**
     * @return mixed
     */
    public function getCasefileCount();

    /**
     * @param $caseID
     *
     * @return mixed
     */
    public function getCasefile($caseID);

    /**
     * @param $caseID
     *
     * @return mixed
     */
    public function getCasefileContacts($caseID);

    /**
     * @param $caseID
     *
     * @return mixed
     */
    public function getCasefileMembers($caseID);

    /**
     * @param $caseID
     *
     * @return mixed
     */
    public function getCasefileQuestionnaires($caseID);

    /**
     * @param $caseID
     * @param $questionnaireID
     *
     * @return mixed
     */
    public function getCasefileQuestionnaire($caseID, $questionnaireID);

    /**
     * @param $caseID
     * @param $questionnaireID
     *
     * @return mixed
     */
    public function getCasefileQuestionnaireResponse($caseID, $questionnaireID);

    /**
     * @param $caseID
     * @param $questionnaireID
     * @param $response
     *
     * @return mixed
     */
    public function saveCasefileQuestionnaireResponse(
        $caseID,
        $questionnaireID,
        $response
    );

    /**
     * @param $caseID
     * @param $questionnaireID
     * @param $response
     *
     * @return mixed
     */
    public function submitCasefileQuestionnaireResponse(
        $caseID,
        $questionnaireID,
        $response
    );
}
