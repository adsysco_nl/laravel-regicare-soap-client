<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts;

interface ActiviteitInterface
{
    /**
     * @return array|null
     */
    public function getActiviteiten();

    /**
     * return array|null.
     */
    public function getActiviteitFilters();

    /**
     * @param $activiteitId
     * @return mixed
     */
    public function getActiviteitItem($activiteitId);

    /**
     * @param $activiteitId
     * @param $attributes
     * @return mixed
     */
    public function activiteitInschrijven($activiteitId, $attributes);

    /**
     * @param $inschrijvingID
     * @return mixed
     */
    public function activiteitUitschrijven($inschrijvingID);

    /**
     * @return mixed
     */
    public function getActiviteitenIngeschreven();
}
