<?php


namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts;

interface VacaturebankInterface
{
    /**
     * @return array|null
     */
    public function getVacatures($filters);

    /**
     * @return mixed
     */
    public function getVacaturesGeplaatst($filters);

    /**
     * @param $filters
     * @return mixed
     */
    public function getVacatureCount($filters);

    /**
     * @param $filters
     * @return mixed
     */
    public function getVacatureCountWithSubscribed($filters);

    /**
     * return array|null.
     */
    public function getVacatureFilters();

    /**
     * @param array $data
     * @return mixed
     */
    public function createVacatureItem($data = []);

    /**
     * @param array $data
     * @return mixed
     */
    public function updateVacatureItem($data = []);

    /**
     * @param $vacatureID
     * @return mixed
     */
    public function deleteVacatureItem($vacatureID);

    /**
     * @param $vacatureId
     * @return mixed
     */
    public function getVacatureItem($vacatureId);

    /**
     * @param $vacatureId
     * @return mixed
     */
    public function sendReaction($vacatureId, $data);

    /**
     * @param $vacatureId
     * @return mixed
     */
    public function cancelReaction($vacatureId);

    /**
     * @param $vacatureId
     * @return mixed
     */
    public function acceptReaction($vacatureID);

    /**
     * @param $vacatureId
     * @return mixed
     */
    public function rejectReaction($vacatureID);

    /**
     * @param $attributes
     * @return mixed
     */
    public function createVacatureContactAanvraag($attributes);

    /**
     * @return mixed
     */
    public function getVacaturebankProfielGegevens();

    /**
     * @return mixed
     */
    public function vacaturebankProfielAfmelden();

    /**
     * @param $attributes
     * @return mixed
     */
    public function updateVacaturebankProfielGegevens($attributes);

    /**
     * @return mixed
     */
    public function getVacaturebankProfielFilters();

    /**
     * @return mixed
     */
    public function getVacaturebankAanbiederFilters();

    /**
     * @return mixed
     */
    public function getVacaturebankConfiguratie();

    /**
     * @return array|null
     */
    public function getReactions();

    /**
     * @return array|null
     */
    public function getVacatureAantal();

    /**
     * @param $volunteerID
     * @return array|null
     */
    public function getVolunteer($volunteerID);

    /**
     * @return array|null
     */
    public function getVolunteers();


    /**
     * @return array|null
     */
    public function getInvites($filter = []);

    /**
     * @param $vacatureID
     * @param $aanbiederID
     * @return array|null
     */
    public function inviteVolunteer($vacatureID, $volunteerID, $attributes);

    /**
     * @param $inviteID
     * @return array|null
     */
    public function cancelInvite($inviteID);

    /**
     * @param $inviteID
     * @return array|null
     */
    public function acceptInvite($inviteID);

    /**
     * @param $inviteID
     * @return array|null
     */
    public function completeInvite($inviteID);

    /**
     * @param $inviteID
     * @return array|null
     */
    public function rejectInvite($inviteID);
}
