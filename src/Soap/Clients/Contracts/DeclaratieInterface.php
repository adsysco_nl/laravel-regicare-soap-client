<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts;

interface DeclaratieInterface
{
    /**
     * @param null $filter
     * @return mixed
     */
    public function getDeclaraties($filter = null);

    /**
     * @param $declaratieID
     * @return mixed
     * @internal param $aanbodId
     */
    public function getDeclaratieItem($declaratieID);

    /**
     * @param $attributes
     * @return mixed
     */
    public function createDeclaratieItem($attributes);

    /**
     * @param $attributes
     * @return mixed
     */
    public function updateDeclaratieItem($attributes);

    /**
     * @param $declaratieID
     * @return mixed
     */
    public function deleteDeclaratieItem($declaratieID);

    /**
     * @param $declaratieID
     * @return mixed
     */
    public function getBonForDeclaratie($declaratieID);

    /**
     * @return mixed
     */
    public function getDeclaratieSoort();
}
