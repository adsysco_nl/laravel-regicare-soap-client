<?php


namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts;

interface ArticleInterface
{
    /**
     * @param  null $filter
     * @return mixed
     */
    public function getPublicArticles($filter = null);

    /**
     * @param  null $filter
     * @return mixed
     */
    public function getAllArticles($filter = null);
}
