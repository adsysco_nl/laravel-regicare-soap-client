<?php


namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts;

use Adsysco\LaravelRegicareSoapClient\Models\Match\APIMatchFilters;
use Adsysco\LaravelRegicareSoapClient\Models\Match\MatchScope;
use Illuminate\Support\Collection;

interface MatchInterface
{
    /**
     * @param  MatchScope  $scope
     * @return mixed
     */
    public function getMatchConfiguratie(MatchScope $scope);

    /**
     * @param  MatchScope  $scope
     * @return mixed
     */
    public function getMatchProfielGegevens(MatchScope $scope);

    /**
     * @param  MatchScope  $scope
     * @param $attributes
     * @return mixed
     */
    public function updateMatchProfielGegevens(MatchScope $scope, $attributes);

    /**
     * @param  MatchScope  $scope
     * @param $attributes
     * @return mixed
     */
    public function registerMatchProfiel(MatchScope $scope, $attributes);

    /**
     * @param  MatchScope  $scope
     * @return mixed
     */
    public function matchProfielAfmelden(MatchScope $scope);

    /**
     * @param  MatchScope  $scope
     * @param $attributes
     * @return mixed
     */
    public function createMatchContactAanvraag(MatchScope $scope, $attributes);

    /**
     * @param  MatchScope  $scope
     * @return mixed
     */
    public function getMatchProfielFilters(MatchScope $scope);

    /**
     * @param  MatchScope  $scope
     * @param $aanbodId
     * @return mixed
     */
    public function getMatchAanbodInformatie(MatchScope $scope, $aanbodId);

    /**
     * @param  MatchScope  $scope
     * @return mixed
     */
    public function getMatchAanbodFilter(MatchScope $scope);

    /**
     * @param  MatchScope  $scope
     * @return mixed
     */
    public function getMatchAanbodGeplaatst(MatchScope $scope);

    /**
     * @param  MatchScope  $scope
     * @return mixed
     */
    public function getMatchAanbodGereageerd(MatchScope $scope);

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return mixed
     */
    public function acceptAanbodReactie(MatchScope $scope, $reactieID);

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return mixed
     */
    public function rejectAanbodReactie(MatchScope $scope, $reactieID);

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return mixed
     */
    public function getMatchAanbodReactieAvatar(MatchScope $scope, $reactieID);

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return mixed
     */
    public function getMatchVraagReactieAvatar(MatchScope $scope, $reactieID);

    /**
     * @param  Collection  $scopes
     * @param  APIMatchFilters|null  $filters
     * @return array|null
     */
    public function getMatchVraagOverzicht(Collection $scopes, APIMatchFilters $filters = null);

    /**
     * @param  Collection  $scopes
     * @param  APIMatchFilters|null  $filters
     * @return array|null
     */
    public function getMatchVraagAantal(Collection $scopes, APIMatchFilters $filters = null);

    /**
     * @param  Collection  $scopes
     * @return mixed
     */
    public function getMatchVraagFilter(Collection $scopes);

    /**
     * @param  MatchScope  $scope
     * @param $vraagId
     * @return mixed
     */
    public function getMatchVraagInformatie(MatchScope $scope, $vraagId);

    /**
     * @param  MatchScope  $scope
     * @param $vraagId
     * @param $data
     * @return mixed
     */
    public function updateVraagItem(MatchScope $scope, $vraagId, $data);

    /**
     * @param  MatchScope  $scope
     * @param $aanbodId
     * @param $data
     * @return mixed
     */
    public function updateAanbodItem(MatchScope $scope, $aanbodId, $data);

    /**
     * @param  MatchScope  $scope
     * @return mixed
     */
    public function getMatchVraagGeplaatst(MatchScope $scope);

    /**
     * @param  MatchScope  $scope
     * @return mixed
     */
    public function getMatchVraagGereageerd(MatchScope $scope);

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return mixed
     */
    public function acceptVraagReactie(MatchScope $scope, $reactieID);

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return mixed
     */
    public function rejectVraagReactie(MatchScope $scope, $reactieID);

    /**
     * @param  MatchScope  $scope
     * @param $vraagID
     * @param $attributes
     * @return mixed
     */
    public function createVraagReactie(MatchScope $scope, $vraagID, $attributes);

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return mixed
     */
    public function cancelVraagReactie(MatchScope $scope, $reactieID);

    /**
     * @param  MatchScope  $scope
     * @param $aanbodID
     * @param $attributes
     * @return mixed
     */
    public function createAanbodReactie(MatchScope $scope, $aanbodID, $attributes);

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return mixed
     */
    public function cancelAanbodReactie(MatchScope $scope, $reactieID);

    /**
     * @param  MatchScope  $scope
     * @param $attributes
     * @return mixed
     */
    public function createVraagItem(MatchScope $scope, $attributes);

    /**
     * @param  MatchScope  $scope
     * @param $attributes
     * @return mixed
     */
    public function createAanbodItem(MatchScope $scope, $attributes);
}
