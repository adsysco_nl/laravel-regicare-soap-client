<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients;

use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\SoapClient;

class LoggingSoapClient implements SoapClient
{
    /**
     * @var RegiService
     */
    private $soap;

    /**
     * CachedSoapClient constructor.
     * @param SoapClient $soap
     */
    public function __construct(SoapClient $soap)
    {
        $this->soap = $soap;
    }

    public function __call($method, $parameters)
    {
        $soapResult = null;

        $this->soap->enableDebug();

        try {
            $soapResult = $this->soap->$method(...$parameters);
        } catch (\Exception $e) {
            throw $e;
        } finally {
            $curl = $this->soap->getLastRequestXDebug();

            \Log::debug(
                $method,
                [
                    'method' => $method,
                    'params' => $parameters,
                    'curl' => $curl,
                    'response' => $soapResult
                ]
            );
        }

        return $soapResult;
    }
}
