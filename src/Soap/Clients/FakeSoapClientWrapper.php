<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients;

use Adsysco\LaravelRegicareSoapClient\Soap\ApiResponse;
use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\SoapClient;

class FakeSoapClientWrapper implements SoapClient
{
    private $soap;

    const FEATURES = [
        'vacatures' => [
            'getVacatures',
            'getVacaturesGeplaatst',
            'getVacatureCount',
            'getVacatureCountWithSubscribed',
            'getVacatureFilters',
            'createVacatureItem',
            'updateVacatureItem',
            'deleteVacatureItem',
            'getVacatureItem',
            'sendReaction',
            'cancelReaction',
            'acceptReaction',
            'rejectReaction',
            'createVacatureContactAanvraag',
            'getVacaturebankProfielGegevens',
            'updateVacaturebankProfielGegevens',
            'getVacaturebankProfielFilters',
            'getVacaturebankAanbiederFilters',
            'getVacaturebankConfiguratie',
            'getReactions',
            'getVacatureAantal',
            'getVolunteer',
            'getVolunteers',
            'getInvites',
            'inviteVolunteer',
            'cancelInvite',
            'acceptInvite',
            'completeInvite',
            'rejectInvite'
        ],
        'prikbord' => [
            'getMarktplaatsAanbodOverzicht',
            'getMarktplaatsVraagOverzicht',
            'getMarktplaatsAanbodInformatie',
            'getMarktplaatsAanbodGeplaatst',
            'getMarktplaatsAanbodGereageerd',
            'acceptAanbodReactie',
            'rejectAanbodReactie',
            'getMarktplaatsAanbodReactieAvatar',
            'getMarktplaatsVraagReactieAvatar',
            'getMarktplaatsVraagInformatie',
            'updateVraagItem',
            'updateAanbodItem',
            'getMarktplaatsVraagGeplaatst',
            'getMarktplaatsVraagGereageerd',
            'acceptVraagReactie',
            'rejectVraagReactie',
            'createVraagReactie',
            'cancelVraagReactie',
            'createAanbodReactie',
            'cancelAanbodReactie',
            'getMarktplaatsConfiguratie',
            'registerMarktplaatsProfiel',
            'getMarktplaatsProfielGegevens',
            'updateMarktplaatsProfielGegevens',
            'getMarktplaatsProfielFilters',
            'getMarktplaatsFilter',
            'createVraagItem',
            'createAanbodItem'
        ]
    ];

    public function __construct($isExternal = null)
    {
        if (isset($isExternal)) {
            $this->isExternal = $isExternal;
        }

        $this->soap = new FakeSoapClient($isExternal);
    }

    public function __call($methodName, $args)
    {
        self::checkFeatures($methodName);

        return (new ApiResponse($this->soap->{$methodName}(...$args)))->getData();
    }

    public function __get(string $name)
    {
        if ($name == 'soap') {
            return $this->soap;
        }

        return $this->soap->{$name};
    }

    private static function checkFeatures($methodName)
    {
        foreach (self::FEATURES as $feature => $methods) {
            if (in_array($methodName, $methods) && !config('features.' . $feature)) {
                return (new ApiResponse(
                    [
                        'error' => 'error',
                        'description' => sprintf('De feature %s is uitgeschakeld!', $feature)
                    ]
                ))->getData();
            }
        }
    }
}
