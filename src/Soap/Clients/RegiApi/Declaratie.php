<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\RegiApi;

use Adsysco\LaravelRegicareSoapClient\Soap\ApiResponse;

trait Declaratie
{
    public function getDeclaraties($filter = null)
    {
        return (new ApiResponse($this->call('declaratieOverzicht', [
            $this->apiKey,
            $this->loginKey(),
            $filter,
        ])))->getData();
    }

    public function getDeclaratieItem($declaratieID)
    {
        return (new ApiResponse($this->call('declaratieGegevens', [
            $this->apiKey,
            $this->loginKey(),
            $declaratieID,
        ])))->getData();
    }

    public function createDeclaratieItem($attributes)
    {
        return (new ApiResponse($this->call('declaratieToevoegen', [
            $this->apiKey,
            $this->loginKey(),
            $attributes,
        ])))->getData();
    }

    public function updateDeclaratieItem($attributes)
    {
        return (new ApiResponse($this->call('declaratieBewerken', [
            $this->apiKey,
            $this->loginKey(),
            $attributes['declaratieID'],
            $attributes,
        ])))->getData();
    }

    public function deleteDeclaratieItem($declaratieID)
    {
        return (new ApiResponse($this->call('declaratieVerwijderen', [
            $this->apiKey,
            $this->loginKey(),
            $declaratieID
        ])))->getData();
    }

    public function getBonForDeclaratie($declaratieID)
    {
        return (new ApiResponse($this->call('declaratieBon', [
            $this->apiKey,
            $this->loginKey(),
            $declaratieID
        ])))->getData();
    }

    public function getDeclaratieSoort()
    {
        return (new ApiResponse($this->call('declaratieSoort', [
            $this->apiKey,
            $this->loginKey()
        ])))->getData();
    }

    public function getDeclaratieOrganisatie()
    {
        return (new ApiResponse($this->call('declaratieOrganisatie', [
            $this->apiKey,
            $this->loginKey()
        ])))->getData();
    }

    public function getDeclaratieClient()
    {
        return (new ApiResponse($this->call('declaratieClient', [
            $this->apiKey,
            $this->loginKey()
        ])))->getData();
    }
}
