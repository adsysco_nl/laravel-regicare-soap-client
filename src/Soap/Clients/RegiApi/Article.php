<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\RegiApi;

use Adsysco\LaravelRegicareSoapClient\Soap\ApiResponse;

trait Article
{
    public function getAllArticles($filter = null)
    {
        return (new ApiResponse(
            $this->call(
                'portalNieuwsOverzicht',
                [
                $this->apiKey,
                $this->loginKey(),
                $filter,
                ]
            )
        ))->getData();
    }

    public function getPublicArticles($filter = null)
    {
        return (new ApiResponse(
            $this->call(
                'portalNieuwsOverzicht',
                [
                $this->apiKey,
                null,
                $filter,
                ]
            )
        ))->getData();
    }

    public function getArticle($articleId)
    {
        return (new ApiResponse(
            $this->call(
                'portalNieuwsInformatie',
                [
                $this->apiKey,
                $this->loginKey(),
                $articleId
                ]
            )
        ))->getData();
    }

    public function getArticleAfbeelding($articleId)
    {
        return (new ApiResponse(
            $this->call(
                'portalNieuwsAfbeelding',
                [
                $this->apiKey,
                $this->loginKey(),
                $articleId
                ]
            )
        ))->getData();
    }
}
