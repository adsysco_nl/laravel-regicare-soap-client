<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\RegiApi;

use Adsysco\LaravelRegicareSoapClient\Soap\ApiResponse;
use Illuminate\Support\Facades\Auth;

trait Vacaturebank
{
    public function getVacatures($filters)
    {
        return (new ApiResponse($this->call('vacaturebankVacatureOverzicht', [
            $this->apiKey,
            $this->werksoortId,
            $this->getLoginKeyIf(function () {
                return Auth::user()->hasVacaturebankProfiel();
            }),
            $filters
        ])))->getData();
    }

    public function getVacaturesGeplaatst($filters)
    {
        return (new ApiResponse($this->call('vacaturebankVacatureGeplaatst', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey(),
            $filters
        ])))->getData();
    }

    public function getVacatureGeplaatstItem($id)
    {
        return (new ApiResponse($this->call('vacaturebankVacatureGegevens', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey(),
            $id
        ])))->getData();
    }

    public function getVacatureTemplate()
    {
        return (new ApiResponse($this->call('vacaturebankVacatureTemplate', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey()
        ])))->getData();
    }

    public function getVacatureFilters()
    {
        return (new ApiResponse($this->call('vacaturebankVacatureFilter', [
            $this->apiKey,
            $this->werksoortId
        ])))->getData();
    }

    public function getVacatureCount($filters)
    {
        return (new ApiResponse($this->call('vacaturebankVacatureOverzichtAantal', [
            $this->apiKey,
            $this->werksoortId,
            $this->getLoginKeyIf(function () {
                return Auth::user()->hasVacaturebankProfiel();
            }),
            $filters
        ])))->getData();
    }

    public function getVacatureCountWithSubscribed($filters)
    {
        return (new ApiResponse($this->call('vacaturebankVacatureOverzichtAantal', [
            $this->apiKey,
            $this->werksoortId,
            null,
            $filters
        ])))->getData();
    }

    public function createVacatureItem($data = [])
    {
        return (new ApiResponse($this->call('vacaturebankVacatureToevoegen', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey(),
            $data
        ])))->getData();
    }

    public function updateVacatureItem($data = [])
    {
        return (new ApiResponse($this->call('vacaturebankVacatureBewerken', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey(),
            $data['vacatureID'],
            $data
        ])))->getData();
    }

    public function deleteVacatureItem($vacatureID)
    {
        return (new ApiResponse($this->call('vacaturebankVacatureVerwijderen', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey(),
            $vacatureID
        ])))->getData();
    }

    public function getVacatureItem($vacatureID)
    {
        return (new ApiResponse($this->call('vacaturebankVacatureInformatie', [
            $this->apiKey,
            $this->werksoortId,
            $vacatureID,
        ])))->getData();
    }

    public function registerVacaturebankProfiel($attributes)
    {
        return (new ApiResponse($this->call('vacaturebankProfielAanmelden', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey(),
            $attributes
        ])))->getData();
    }

    public function getVacaturebankProfielStatus()
    {
        return (new ApiResponse($this->call('vacaturebankProfielStatus', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey()
        ])))->getData();
    }

    public function getVacaturebankProfielGegevens()
    {
        return (new ApiResponse($this->call('vacaturebankProfielGegevens', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey()
        ])))->getData();
    }

    public function vacaturebankProfielAfmelden()
    {
        return (new ApiResponse($this->call('vacaturebankProfielAfmelden', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey()
        ])))->getData();
    }

    public function updateVacaturebankProfielGegevens($attributes)
    {
        return (new ApiResponse($this->call('vacaturebankProfielOpslaan', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey(),
            $attributes
        ])))->getData();
    }

    public function getVacaturebankProfielFilters()
    {
        return (new ApiResponse($this->call('vacaturebankProfielFilter', [
            $this->apiKey,
            $this->werksoortId,
        ])))->getData();
    }

    public function getVacaturebankAanbiederFilters()
    {
        return (new ApiResponse($this->call('vacaturebankAanbiederFilter', [
            $this->apiKey,
            $this->werksoortId,
        ])))->getData();
    }

    public function getVacaturebankConfiguratie()
    {
        return (new ApiResponse($this->call('vacaturebankConfiguratie', [
            $this->apiKey,
            $this->werksoortId,
        ])))->getData();
    }

    public function createVacatureContactAanvraag($attributes)
    {
        return (new ApiResponse($this->call('vacaturebankContactAanvraag', [
            $this->apiKey,
            $this->werksoortId,
            $attributes,
        ])))->getData();
    }

    public function getReactions($filters = [])
    {
        return (new ApiResponse($this->call('vacaturebankVacatureIngeschreven', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey(),
            $filters
        ])))->getData();
    }

    public function sendReaction($vacatureID, $data = [])
    {
        return (new ApiResponse(
            $this->soapWrapper->call(
                'API' . $this->name . '.vacaturebankVacatureInschrijvingPlaatsen',
                [
                    $this->apiKey,
                    $this->werksoortId,
                    $this->loginKey(),
                    $vacatureID,
                    $data
                ]
            )
        ))->getData();
    }

    public function cancelReaction($reactionID)
    {
        return (new ApiResponse(
            $this->soapWrapper->call(
                'API' . $this->name . '.vacaturebankVacatureInschrijvingAnnuleren',
                [
                    $this->apiKey,
                    $this->werksoortId,
                    $this->loginKey(),
                    $reactionID
                ]
            )
        ))->getData();
    }

    public function acceptReaction($inviteID)
    {
        return (new ApiResponse(
            $this->soapWrapper->call(
                'API' . $this->name . '.vacaturebankVacatureInschrijvingVoltooien',
                [
                    $this->apiKey,
                    $this->werksoortId,
                    $this->loginKey(),
                    $inviteID
                ]
            )
        ))->getData();
    }

    public function rejectReaction($reactionID)
    {
        return (new ApiResponse(
            $this->soapWrapper->call(
                'API' . $this->name . '.vacaturebankVacatureInschrijvingAnnuleren',
                [
                    $this->apiKey,
                    $this->werksoortId,
                    $this->loginKey(),
                    $reactionID
                ]
            )
        ))->getData();
    }

    public function getVacatureAantal()
    {
        return (new ApiResponse(
            $this->soapWrapper->call(
                'API' . $this->name . '.vacaturebankVacatureOverzichtAantal',
                [
                    $this->apiKey,
                    $this->werksoortId,
                    $this->getLoginKeyIf(function () {
                        return Auth::user()->hasVacaturebankProfiel();
                    }),
                ]
            )
        ))->getData();
    }

    public function getVolunteer($volunteerID)
    {
        return (new ApiResponse(
            $this->soapWrapper->call(
                'API' . $this->name . '.vacaturebankAanbiederInformatie',
                [
                    $this->apiKey,
                    $this->werksoortId,
                    $volunteerID
                ]
            )
        ))->getData();
    }

    public function getVolunteers()
    {
        return (new ApiResponse(
            $this->soapWrapper->call(
                'API' . $this->name . '.vacaturebankAanbiederOverzicht',
                [
                    $this->apiKey,
                    $this->werksoortId
                ]
            )
        ))->getData();
    }

    public function getInvites($filter = [])
    {
        return (new ApiResponse($this->call('vacaturebankAanbiederIngeschreven', [
            $this->apiKey,
            $this->werksoortId,
            $this->loginKey(),
            $filter
        ])))->getData();
    }

    public function inviteVolunteer($vacatureID, $volunteerID, $attributes = [])
    {
        return (new ApiResponse(
            $this->soapWrapper->call(
                'API' . $this->name . '.vacaturebankAanbiederInschrijvingPlaatsen',
                [
                    $this->apiKey,
                    $this->werksoortId,
                    $this->loginKey(),
                    $vacatureID,
                    $volunteerID,
                    $attributes
                ]
            )
        ))->getData();
    }

    public function cancelInvite($inviteID)
    {
        return (new ApiResponse(
            $this->soapWrapper->call(
                'API' . $this->name . '.vacaturebankAanbiederInschrijvingAnnuleren',
                [
                    $this->apiKey,
                    $this->werksoortId,
                    $this->loginKey(),
                    $inviteID
                ]
            )
        ))->getData();
    }

    public function acceptInvite($inviteID)
    {
        return (new ApiResponse(
            $this->soapWrapper->call(
                'API' . $this->name . '.vacaturebankAanbiederInschrijvingAccepteren',
                [
                    $this->apiKey,
                    $this->werksoortId,
                    $this->loginKey(),
                    $inviteID
                ]
            )
        ))->getData();
    }

    public function completeInvite($inviteID)
    {
        return (new ApiResponse(
            $this->soapWrapper->call(
                'API' . $this->name . '.vacaturebankAanbiederInschrijvingVoltooien',
                [
                    $this->apiKey,
                    $this->werksoortId,
                    $this->loginKey(),
                    $inviteID
                ]
            )
        ))->getData();
    }

    public function rejectInvite($inviteID)
    {
        return (new ApiResponse(
            $this->soapWrapper->call(
                'API' . $this->name . '.vacaturebankAanbiederInschrijvingAfwijzen',
                [
                    $this->apiKey,
                    $this->werksoortId,
                    $this->loginKey(),
                    $inviteID
                ]
            )
        ))->getData();
    }
}
