<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\RegiApi;

use Adsysco\LaravelRegicareSoapClient\Soap\ApiResponse;

trait Casefile
{
    public function getCasefiles()
    {
        return (new ApiResponse($this->soapWrapper->call('API' . $this->name . '.casusOverzicht', [
            $this->apiKey,
            $this->loginKey(),
        ])))->getData();
    }

    public function getCasefileCount()
    {
        return (new ApiResponse($this->soapWrapper->call('API' . $this->name . '.casusOverzichtAantal', [
            $this->apiKey,
            $this->loginKey(),
        ])))->getData();
    }

    public function getCasefile($caseID)
    {
        return (new ApiResponse($this->soapWrapper->call('API' . $this->name . '.casusGegevens', [
            $this->apiKey,
            $this->loginKey(),
            $caseID
        ])))->getData();
    }

    public function getCasefileContacts($caseID)
    {
        return (new ApiResponse($this->soapWrapper->call('API' . $this->name . '.casusContactmomenten', [
            $this->apiKey,
            $this->loginKey(),
            $caseID
        ])))->getData();
    }

    public function getCasefileMembers($caseID)
    {
        return (new ApiResponse($this->soapWrapper->call('API' . $this->name . '.casusKader', [
            $this->apiKey,
            $this->loginKey(),
            $caseID
        ])))->getData();
    }

    public function getCasefileQuestionnaires($caseID)
    {
        return (new ApiResponse($this->soapWrapper->call('API' . $this->name . '.casusVragenlijstOverzicht', [
            $this->apiKey,
            $this->loginKey(),
            $caseID,
        ])))->getData();
    }

    public function getCasefileQuestionnaire($caseID, $questionnaireID)
    {
        return (new ApiResponse($this->soapWrapper->call('API' . $this->name . '.casusVragenlijstGegevens', [
            $this->apiKey,
            $this->loginKey(),
            $caseID,
            $questionnaireID
        ])))->getData();
    }

    public function getCasefileQuestionnaireResponse($caseID, $questionnaireID)
    {
        return (new ApiResponse($this->soapWrapper->call('API' . $this->name . '.casusVragenlijstAntwoorden', [
            $this->apiKey,
            $this->loginKey(),
            $caseID,
            $questionnaireID
        ])))->getData();
    }

    public function saveCasefileQuestionnaireResponse($caseID, $questionnaireID, $response)
    {
        return (new ApiResponse($this->soapWrapper->call('API' . $this->name . '.casusVragenlijstOpslaan', [
            $this->apiKey,
            $this->loginKey(),
            $caseID,
            $questionnaireID,
            $response
        ])))->getData();
    }

    public function submitCasefileQuestionnaireResponse($caseID, $questionnaireID, $response)
    {
        return (new ApiResponse($this->soapWrapper->call('API' . $this->name . '.casusVragenlijstVerzenden', [
            $this->apiKey,
            $this->loginKey(),
            $caseID,
            $questionnaireID,
            $response
        ])))->getData();
    }
}
