<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\RegiApi;

use Adsysco\LaravelRegicareSoapClient\Soap\ApiResponse;

trait Activiteit
{
    public function getActiviteiten()
    {
        return (new ApiResponse($this->call('activiteitOverzicht', [
            $this->apiKey,
            null,
            config('client.activiteiten_filter')
        ])))->getData();
    }

    public function getActiviteitFilters()
    {
        return (new ApiResponse($this->call('activiteitFilter', [
            $this->apiKey
        ])))->getData();
    }

    public function getActiviteitItem($activiteitId)
    {
        return (new ApiResponse($this->call('activiteitInformatie', [
            $this->apiKey,
            $activiteitId,
        ])))->getData();
    }

    public function activiteitInschrijven($activiteitId, $attributes)
    {
        return (new ApiResponse($this->call('activiteitInschrijvingPlaatsen', [
            $this->apiKey,
            $this->loginKey(),
            $activiteitId,
            $attributes
        ])))->getData();
    }

    public function activiteitUitschrijven($inschrijvingID)
    {
        return (new ApiResponse($this->call('activiteitInschrijvingAnnuleren', [
            $this->apiKey,
            $this->loginKey(),
            $inschrijvingID
        ])))->getData();
    }

    public function getActiviteitenIngeschreven()
    {
        return (new ApiResponse($this->call('activiteitIngeschreven', [
            $this->apiKey,
            $this->loginKey(),
        ])))->getData();
    }

    public function getActiviteitAantal()
    {
        return (new ApiResponse($this->call('activiteitOverzichtAantal', [
            $this->apiKey,
            $this->loginKey(),
            config('client.activiteiten_filter')
        ])))->getData();
    }

    public function getBijeenkomsten($activiteitID)
    {
        return (new ApiResponse($this->call('activiteitBijeenkomstOverzicht', [
            $this->apiKey,
            ['activiteit' => [$activiteitID]]
        ])))->getData();
    }
}
