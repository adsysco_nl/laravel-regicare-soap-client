<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\RegiApi;

use Adsysco\LaravelRegicareSoapClient\Models\Match\MatchScope;
use Adsysco\LaravelRegicareSoapClient\Models\Match\APIMatchFilters;
use Adsysco\LaravelRegicareSoapClient\Soap\ApiResponse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

/**
 * Trait RegiMatch
 * @package App\Soap\Clients\RegiApi
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 */
trait RegiMatch
{
    public function getMatchConfiguratie(MatchScope $scope)
    {
        $endpointMarktplaats = 'marktplaatsConfiguratie';
        $endpointPraktischeHulp = 'praktischehulpConfiguratie';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
        ])))->getData();
    }

    public function createMatchContactAanvraag(MatchScope $scope, $attributes)
    {
        $endpointMarktplaats = 'marktplaatsContactAanvraag';
        $endpointPraktischeHulp = 'praktischehulpContactAanvraag';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $attributes,
        ])))->getData();
    }

    public function registerMatchProfiel(MatchScope $scope, $attributes)
    {
        $endpointMarktplaats = 'marktplaatsProfielAanmelden';
        $endpointPraktischeHulp = [
            MatchScope::ROLE_CLIENT => 'praktischehulpVragerProfielAanmelden',
            MatchScope::ROLE_VOLUNTEER => 'praktischehulpVrijwilligerProfielAanmelden',
        ];
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $attributes
        ])))->getData();
    }

    public function getMatchProfielStatus(MatchScope $scope)
    {
        $endpointMarktplaats = 'marktplaatsProfielStatus';
        $endpointPraktischeHulp = [
            MatchScope::ROLE_CLIENT => 'praktischehulpVragerProfielStatus',
            MatchScope::ROLE_VOLUNTEER => 'praktischehulpVrijwilligerProfielStatus',
        ];
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
        ])))->getData();
    }

    public function getMatchProfielGegevens(MatchScope $scope)
    {
        $endpointMarktplaats = 'marktplaatsProfielGegevens';
        $endpointPraktischeHulp = [
            MatchScope::ROLE_CLIENT => 'praktischehulpVragerProfielGegevens',
            MatchScope::ROLE_VOLUNTEER => 'praktischehulpVrijwilligerProfielGegevens',
        ];
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
        ])))->getData();
    }

    public function matchProfielAfmelden(MatchScope $scope)
    {
        $endpointMarktplaats = 'marktplaatsProfielAfmelden';
        $endpointPraktischeHulp = [
            MatchScope::ROLE_CLIENT => 'praktischehulpVragerProfielAfmelden',
            MatchScope::ROLE_VOLUNTEER => 'praktischehulpVrijwilligerProfielAfmelden',
        ];
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey()
        ])))->getData();
    }

    public function updateMatchProfielGegevens(MatchScope $scope, $attributes)
    {
        $endpointMarktplaats = 'marktplaatsProfielOpslaan';
        $endpointPraktischeHulp = [
            MatchScope::ROLE_CLIENT => 'praktischehulpVragerProfielOpslaan',
            MatchScope::ROLE_VOLUNTEER => 'praktischehulpVrijwilligerProfielOpslaan',
        ];
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $attributes
        ])))->getData();
    }

    public function getMatchProfielFilters(MatchScope $scope)
    {
        $endpointMarktplaats = 'marktplaatsProfielFilter';
        $endpointPraktischeHulp = [
            MatchScope::ROLE_CLIENT => 'praktischehulpVragerProfielFilter',
            MatchScope::ROLE_VOLUNTEER => 'praktischehulpVrijwilligerProfielFilter',
        ];
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
        ])))->getData();
    }

    public function getMatchVraagOverzicht(Collection $scopes, APIMatchFilters $filters = null)
    {
        $requestFilters = $filters !== null ? $filters->toArray() : [];
        return (new ApiResponse($this->call('matchVraagOverzicht', [
            $this->apiKey,
            $scopes->pluck('id')->toArray(),
            $this->loginKey(),
            $requestFilters
        ])))->getData();
    }

    public function getMatchAanbodInformatie(MatchScope $scope, $aanbodId)
    {
        if ($scope->isPraktischeHulp()) {
            return (new ApiResponse($this->call('praktischehulpAanbodGegevens', [
                $this->apiKey,
                $scope->id,
                $this->loginKey(),
                $aanbodId,
            ])))->getData();
        }

        return (new ApiResponse($this->call('marktplaatsAanbodInformatie', [
            $this->apiKey,
            $scope->id,
            $aanbodId,
        ])))->getData();
    }

    public function getMatchAanbodGeplaatst(MatchScope $scope)
    {
        $endpointMarktplaats = 'marktplaatsAanbodGeplaatst';
        $endpointPraktischeHulp = 'praktischehulpAanbodGeplaatst';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey()
        ])))->getData();
    }

    public function getMatchAanbodGereageerd(MatchScope $scope)
    {
        $endpointMarktplaats = 'marktplaatsAanbodGereageerd';
        $endpointPraktischeHulp = 'praktischehulpAanbodGereageerd';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey()
        ])))->getData();
    }

    public function acceptAanbodReactie(MatchScope $scope, $reactieID)
    {
        $endpointMarktplaats = 'marktplaatsAanbodReactieAccepteren';
        $endpointPraktischeHulp = 'praktischehulpAanbodReactieAccepteren';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $reactieID
        ])))->getData();
    }

    public function rejectAanbodReactie(MatchScope $scope, $reactieID)
    {
        $endpointMarktplaats = 'marktplaatsAanbodReactieAfwijzen';
        $endpointPraktischeHulp = 'praktischehulpAanbodReactieAfwijzen';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $reactieID
        ])))->getData();
    }

    public function getMatchAanbodReactieAvatar(MatchScope $scope, $reactieID)
    {
        $endpointMarktplaats = 'marktplaatsAanbodReactieAvatar';
        $endpointPraktischeHulp = 'praktischehulpAanbodReactieAvatar';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $reactieID
        ])))->getData();
    }

    public function getMatchVraagReactieAvatar(MatchScope $scope, $reactieID)
    {
        $endpointMarktplaats = 'marktplaatsVraagReactieAvatar';
        $endpointPraktischeHulp = 'praktischehulpVraagReactieAvatar';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $reactieID
        ])))->getData();
    }

    public function getMatchVraagInformatie(MatchScope $scope, $vraagId, $scopeId = null)
    {
        $endpointMarktplaats = 'marktplaatsVraagInformatie';
        $endpointPraktischeHulp = 'praktischehulpVraagInformatie';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scopeId ?? $scope->id,
            $vraagId,
        ])))->getData();
    }

    public function updateVraagItem(MatchScope $scope, $vraagId, $data)
    {
        $endpointMarktplaats = 'marktplaatsVraagBewerken';
        $endpointPraktischeHulp = 'praktischehulpVraagBewerken';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $vraagId,
            $data
        ])))->getData();
    }

    public function updateAanbodItem(MatchScope $scope, $aanbodId, $data)
    {
        $endpointMarktplaats = 'marktplaatsAanbodBewerken';
        $endpointPraktischeHulp = 'praktischehulpAanbodBewerken';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $aanbodId,
            $data
        ])))->getData();
    }

    public function getMatchVraagGeplaatst(MatchScope $scope)
    {
        $endpointMarktplaats = 'marktplaatsVraagGeplaatst';
        $endpointPraktischeHulp = 'praktischehulpVraagGeplaatst';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey()
        ])))->getData();
    }

    public function getMatchVraagGereageerd(MatchScope $scope)
    {
        $endpointMarktplaats = 'marktplaatsVraagGereageerd';
        $endpointPraktischeHulp = 'praktischehulpVraagGematcht';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey()
        ])))->getData();
    }

    public function acceptVraagReactie(MatchScope $scope, $reactieID)
    {
        $endpointMarktplaats = 'marktplaatsVraagReactieAccepteren';
        $endpointPraktischeHulp = 'praktischehulpVraagReactieAccepteren';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $reactieID
        ])))->getData();
    }

    public function rejectVraagReactie(MatchScope $scope, $reactieID)
    {
        $endpointMarktplaats = 'marktplaatsVraagReactieAfwijzen';
        $endpointPraktischeHulp = 'praktischehulpVraagReactieAfwijzen';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $reactieID
        ])))->getData();
    }

    public function acceptVraagUitnodiging(MatchScope $scope, $reactieID)
    {
        return (new ApiResponse($this->call('praktischehulpVraagUitnodigingAccepteren', [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $reactieID
        ])))->getData();
    }

    public function rejectVraagUitnodiging(MatchScope $scope, $reactieID)
    {
        return (new ApiResponse($this->call('praktischehulpVraagUitnodigingAfwijzen', [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $reactieID
        ])))->getData();
    }

    public function createVraagReactie(MatchScope $scope, $vraagID, $attributes)
    {
        $endpointMarktplaats = 'marktplaatsVraagReactiePlaatsen';
        $endpointPraktischeHulp = 'praktischehulpVraagReactiePlaatsen';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $vraagID,
            $attributes
        ])))->getData();
    }

    public function createAanbodReactie(MatchScope $scope, $aanbodID, $attributes)
    {
        $endpointMarktplaats = 'marktplaatsAanbodReactiePlaatsen';
        $endpointPraktischeHulp = 'praktischehulpAanbodReactiePlaatsen';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $aanbodID,
            $attributes
        ])))->getData();
    }

    public function cancelVraagReactie(MatchScope $scope, $reactieID)
    {
        $endpointMarktplaats = 'marktplaatsVraagReactieAnnuleren';
        $endpointPraktischeHulp = 'praktischehulpVraagReactieAnnuleren';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $reactieID
        ])))->getData();
    }

    public function cancelAanbodReactie(MatchScope $scope, $reactieID)
    {
        $endpointMarktplaats = 'marktplaatsAanbodReactieAnnuleren';
        $endpointPraktischeHulp = 'praktischehulpAanbodReactieAnnuleren';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $reactieID
        ])))->getData();
    }

    public function postCommentVraag(MatchScope $scope, $vraagId, $comment)
    {
        $endpointMarktplaats = 'marktplaatsVraagReactiePlaatsen';
        $endpointPraktischeHulp = 'praktischehulpVraagReactiePlaatsen';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);
        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $vraagId,
            $comment
        ])))->getData();
    }

    public function getMatchVraagAantal(Collection $scopes, APIMatchFilters $filters = null)
    {
        $requestFilters = $filters ? $filters->toArray() : [];
        return (new ApiResponse($this->call('matchVraagOverzichtAantal', [
            $this->apiKey,
            $scopes->pluck('id')->toArray(),
            $this->loginKey(),
            $requestFilters
        ])))->getData();
    }

    public function getMatchAanbodFilter(MatchScope $scope)
    {
        $endpointMarktplaats = 'marktplaatsAanbodFilter';
        $endpointPraktischeHulp = 'praktischeHulpAanbodFilter';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);

        return (new ApiResponse($this->call($endpoint, [
            $this->apiKey,
            $scope->id,
        ])))->getData();
    }

    public function getMatchVraagFilter(Collection $scopes)
    {
        return (new ApiResponse($this->call('matchVraagFilter', [
            $this->apiKey,
            $scopes->pluck('id')->toArray(),
        ])))->getData();
    }

    public function createVraagItem(MatchScope $scope, $data)
    {
        $endpointMarktplaats = 'marktplaatsVraagToevoegen';
        $endpointPraktischeHulp = 'praktischehulpVraagToevoegen';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);

        return $this->getFromAPI($scope, $endpoint, $data);
    }

    public function deleteVraagItem(MatchScope $scope, $vraagID)
    {
        $endpointMarktplaats = 'marktplaatsVraagVerwijderen';
        $endpointPraktischeHulp = 'praktischehulpVraagAnnuleren';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);

        return $this->getFromAPI($scope, $endpoint, $vraagID);
    }

    public function createAanbodItem(MatchScope $scope, $data)
    {
        $endpointMarktplaats = 'marktplaatsAanbodToevoegen';
        $endpointPraktischeHulp = 'praktischehulpAanbodToevoegen';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);

        return $this->getFromAPI($scope, $endpoint, $data);
    }

    public function deleteAanbodItem(MatchScope $scope, $aanbodID)
    {
        $endpointMarktplaats = 'marktplaatsAanbodVerwijderen';
        $endpointPraktischeHulp = 'praktischehulpAanbodBeeindigen';
        $endpoint = $this->getEndpointForScope($scope, $endpointMarktplaats, $endpointPraktischeHulp);

        return $this->getFromAPI($scope, $endpoint, $aanbodID);
    }

    public function getMatchAanbodAantal(MatchScope $scope)
    {
        return (new ApiResponse($this->call('marktplaatsAanbodOverzichtAantal', [
            $this->apiKey,
            $scope->id,
            $this->getLoginKeyIf(function () use ($scope) {
                return Auth::user()->hasProfileForScope($scope);
            }),
        ])))->getData();
    }

    protected function getEndpointForScope($scope, $endpointMmarktplaats, $endpointPraktischeHulp)
    {
        switch ($scope->type) {
            case MatchScope::TYPE_MARKTPLAATS:
                return $endpointMmarktplaats;
                break;
            case MatchScope::TYPE_PRAKTISCHE_HULP:
                return $this->getPraktischeHulpEndpointForScope($scope, $endpointPraktischeHulp);
                break;
            default:
                throw new RuntimeException('Invalid scope type: '.$scope->type);
        }
    }

    protected function getPraktischeHulpEndpointForScope($scope, $endpoints)
    {
        if (!is_array($endpoints)) {
            return $endpoints;
        }
        return $endpoints[$scope->role];
    }

    /**
     * @param  MatchScope  $scope
     * @param $endpoint
     * @param $data
     * @return array
     */
    private function getFromAPI(MatchScope $scope, $endpoint, $data): array
    {
        return (new ApiResponse($this->soapWrapper->call('API' . $this->name. '.' . $endpoint, [
            $this->apiKey,
            $scope->id,
            $this->loginKey(),
            $data,
        ])))->getData();
    }
}
