<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Spatie\Once\Cache as RequestCache;
use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\SoapClient;
use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\RegiService;

class CachedSoapClient implements SoapClient
{
    /**
     * @var RegiService
     */
    private $soap;

    /**
     * CachedSoapClient constructor.
     * @param SoapClient $soap
     */
    public function __construct(SoapClient $soap)
    {
        $this->soap = $soap;
    }

    public function __call($method, $parameters)
    {
        $hash = $this->generateHash($method, $parameters);
        $this->clearInvalidCache($method, $hash);

        if ($this->shouldBeCached($method)) {
            if (RequestCache::has($hash, $hash)) {
                return RequestCache::get($hash, $hash);
            }
            if (Cache::has($hash)) {
                return Cache::get($hash);
            }
        }

        $soapResult = $this->soap->$method(...$parameters);

        if ($this->shouldBeCachedForever($method)) {
            Cache::add($hash, $soapResult);
        }

        if ($this->shouldBeCachedForPeriod($method)) {
            Cache::add($hash, $soapResult, now()->addMinutes(config('cache.soap.time')));
        }

        if ($this->shouldBeCachedForRequest($method)) {
            RequestCache::set($hash, $hash, $soapResult);
        }

        return $soapResult;
    }

    private function clearInvalidCache($method, $hash)
    {
        if (! $this->shouldBeCached($method)) {
            if (Cache::has($hash)) {
                Cache::forget($hash);
            }
        }
    }

    private function shouldBeCached($method)
    {
        return $this->shouldBeCachedForPeriod($method) ||
               $this->shouldBeCachedForRequest($method) ||
               $this->shouldBeCachedForever($method);
    }

    /**
     * @param $method
     * @return bool
     */
    private function shouldBeCachedForPeriod($method)
    {
        if (! config('cache.soap.enabled', false)) {
            return false;
        }
        return in_array($method, config('cache.soap.time-based', []));
    }

    /**
     * @param $method
     * @return bool
     */
    private function shouldBeCachedForRequest($method)
    {
        if (! config('cache.soap.enabled', false)) {
            return false;
        }
        return !$this->shouldBeCachedForPeriod($method) && !$this->shouldBeCachedForever($method);
    }

    /**
     * @param $method
     * @return bool
     */
    private function shouldBeCachedForever($method)
    {
        if (! config('cache.soap.enabled', false)) {
            return false;
        }
        return in_array($method, config('cache.soap.forever', []));
    }

    private function generateHash($method, $arguments)
    {
        // @todo: CachedSoapClientTest fails if we use this commented implementation
//         return md5($method.serialize($arguments));

        $arguments = $this->convertCollections($arguments);
        $normalizedArguments = array_map(function ($argument) {
            return is_object($argument) ? spl_object_hash($argument) : $argument;
        }, $arguments);

        return md5($method.serialize($normalizedArguments));
    }

    private function convertCollections($arguments)
    {
        return collect($arguments)->map(function ($argument) {
            if (is_object($argument) && get_class($argument) === Collection::class) {
                return $argument->map(function ($item) {
                    return $item->toArray();
                })->toArray();
            }
            return $argument;
        })->toArray();
    }
}
