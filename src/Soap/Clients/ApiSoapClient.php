<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients;

use Adsysco\LaravelRegicareSoapClient\Soap\ApiResponse;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\RegiApi\Casefile;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts\CasefileInterface;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts\ActiviteitInterface;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts\DeclaratieInterface;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts\MatchInterface;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts\ArticleInterface;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts\VacaturebankInterface;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\RegiApi\Activiteit;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\RegiApi\Declaratie;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\RegiApi\RegiMatch;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\RegiApi\Article;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\RegiApi\Vacaturebank;
use Artisaninweb\SoapWrapper\Client;
use Illuminate\Support\Facades\Auth;
use Artisaninweb\SoapWrapper\Service;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\SoapClient;
use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\RegiService;
use Artisaninweb\SoapWrapper\Exceptions\ServiceAlreadyExists;
use Illuminate\Support\Str;

/**
 * Class ApiSoapClient
 * @package App\Soap\Clients
 *
 * @codeCoverageIgnore
 */
class ApiSoapClient implements
    SoapClient,
    RegiService,
    ActiviteitInterface,
    DeclaratieInterface,
    ArticleInterface,
    VacaturebankInterface,
    MatchInterface,
    CasefileInterface
{
    use Activiteit;
    use Declaratie;
    use Article;
    use Vacaturebank;
    use RegiMatch;
    use Casefile;

    /**
     * @var SoapWrapper
     */
    private $soapWrapper;
    private $apiKey;
    private $wsdl;
    public $isExternal;
    public $name;
    public $siteUrl;
    public $werksoortId;
    public $vacatureFilters;

    public function __construct(
        $apiKey,
        $wsdl,
        $isExternal,
        $name,
        $siteUrl,
        $werksoortId,
        $vacatureFilters,
        SoapWrapper $soapWrapper = null
    ) {
        $this->soapWrapper = $soapWrapper;
        $this->apiKey = $apiKey;
        $this->isExternal = $isExternal;
        $this->name = $name;
        $this->siteUrl = $siteUrl;
        $this->werksoortId = $werksoortId;
        $this->vacatureFilters = $vacatureFilters;

        try {
            $this->soapWrapper->add('API' . $this->name, function (Service $service) use ($wsdl) {
                $service->wsdl($wsdl)->trace(true);
            });
        } catch (ServiceAlreadyExists $e) {
//            Do something
        }
        $this->wsdl = $wsdl;
    }

    /**
    * @return mixed
    */
    protected function loginKey()
    {
        if (!Auth::check()) {
            return null;
        }
        if (session('regicare.loginKey')) {
            return session('regicare.loginKey');
        }
        // @todo: reflect on the user to see if it has the getLoginKey() method.
        return Auth::user()->getLoginKey();
        return Auth::user()->getAuthIdentifier();
    }

    /**
     * Only provide a loginKey if the callback returns true.
     *
     * @param $callback
     * @return mixed|null
     */
    public function getLoginKeyIf($callback)
    {
        if (!Auth::check()) {
            return null;
        }
        return $callback() ? $this->loginKey() : null;
    }

    /**
     * Returns the status of the API.
     * @return array
     */
    public function status()
    {
        return (new ApiResponse($this->call('status', [$this->apiKey])))->getData();
    }

    /**
     * Logs the user in and returns the login information.
     *
     * @param $username
     * @param $password
     * @return mixed
     */
    public function login($username, $password)
    {
        return (new ApiResponse($this->call('login', [
            $this->apiKey,
            $username,
            $password
        ])))->getData();
    }

    /**
     * Logs the user out
     *
     * @return mixed
     */
    public function logout()
    {
        return (new ApiResponse($this->call('logout', [
            $this->apiKey,
            $this->loginKey()
        ])))->getData();
    }

    /**
     * @param array $accountInformation
     * @return array
     */
    public function register(array $accountInformation)
    {
        return (new ApiResponse($this->call('profielAanmelden', [
            $this->apiKey,
            $accountInformation
        ])))->getData();
    }

    /**
     * Get the profile data for a loginKey.
     *
     * @param $loginKey
     * @return array
     */
    public function profile($loginKey)
    {
        return (new ApiResponse($this->call('profielGegevens', [
            $this->apiKey,
            $loginKey
        ])))->getData();
    }

    /**
     * Update  profile data for a loginKey.
     *
     * @param $attributes
     * @return array
     */
    public function updateProfile($attributes)
    {
        return (new ApiResponse($this->call('profielOpslaan', [
            $this->apiKey,
            $this->loginKey(),
            $attributes
        ])))->getData();
    }

    public function createProfielContactAanvraag($attributes)
    {
        return (new ApiResponse($this->call('profielContactAanvraag', [
            $this->apiKey,
            $attributes
        ])))->getData();
    }

    /**
     * Get the linked organization(s) data for a loginKey.
     *
     * @return array
     */
    public function getLinkedOrganizations()
    {
        return (new ApiResponse($this->call('organisatieGekoppeld', [
            $this->apiKey,
            $this->loginKey()
        ])))->getData();
    }

    /**
     * Get the location(s) data for a organizationID.
     * @param $organizationID
     * @return array
     */
    public function getLinkedLocations($organizationID)
    {
        return (new ApiResponse($this->call('organisatieLocatieGekoppeld', [
            $this->apiKey,
            $this->loginKey(),
            $organizationID
        ])))->getData();
    }

    /**
     * Get the employee(s) data for a organizationID.
     *
     * @param  $organizationID
     * @return array
     */
    public function getLinkedEmployees($organizationID)
    {
        return (new ApiResponse($this->call('organisatieMedewerkerGekoppeld', [
            $this->apiKey,
            $this->loginKey(),
            $organizationID
        ])))->getData();
    }

    /**
     * Get the avatar data for a loginKey.
     *
     * @return array
     */
    public function avatar()
    {
        return (new ApiResponse($this->call('profielAvatarGegevens', [
            $this->apiKey,
            $this->loginKey()
        ])))->getData();
    }

    /**
     * Update avatar data for a loginKey.
     *
     * @param $attributes
     * @return array
     */
    public function updateAvatar($attributes)
    {
        return (new ApiResponse($this->call('profielAvatarOpslaan', [
            $this->apiKey,
            $this->loginKey(),
            $attributes
        ])))->getData();
    }

    /**
     * Verify user credentials.
     *
     * @param $credentials
     * @return bool
     */
    public function verify($credentials)
    {
        $verification = $this->call('verify', [
            $this->apiKey,
            $credentials['email'],
            $credentials['password']
        ]);
        return $verification['result'] === 'OK';
    }

    public function getAddressFromPostal($postalCode, $streetNumber)
    {
        return (new ApiResponse($this->call('profielAdresOphalen', [
            $this->apiKey,
            $postalCode,
            $streetNumber
        ])))->getData();
    }

    /**
     * @param String $username
     * @return array
     */
    public function checkUsername($username)
    {
        return (new ApiResponse($this->call('profielGebruikersnaamControle', [
            $this->apiKey,
            $username
        ])))->getData();
    }

    /**
     * @param String $username
     * @return string
     */
    public function resetPassword($username)
    {
        return (new ApiResponse($this->call('profielWachtwoordAanvraag', [
            $this->apiKey,
            $username
        ])))->getData();
    }

    /**
     * @return mixed
     */
    public function profielAfmelden()
    {
        return (new ApiResponse($this->call('profielAfmelden', [
            $this->apiKey,
            $this->loginKey()
        ])))->getData();
    }

    public function call($endpoint, $arguments)
    {
        $maxAttempts = 3;
        $attempts = 0;
        do {
            try {
                return $this->soapWrapper->call('API' . $this->name . '.' . $endpoint, $arguments);
            } catch (\SoapFault $exception) {
                $attempts++;
                if ($attempts === $maxAttempts) {
                    throw $exception;
                }
                sleep(1);
                continue;
            }
            break;
        } while ($attempts < $maxAttempts);
    }

    /**
     * @param mixed $apiKey
     * @return ApiSoapClient
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    public function enableDebug()
    {
        $this->soapWrapper->client('API', function (Client $client) {
            $client->cookie('XDEBUG_SESSION', config('app.xdebug_key'));
        });
    }

    public function getLastRequest()
    {
        return $this->soapWrapper->client('API', function (Client $client) {
            return $client->getLastRequest();
        });
    }

    public function getLastRequestHeaders()
    {
        return $this->soapWrapper->client('API', function (Client $client) {
            return $client->getLastRequestHeaders();
        });
    }

    public function getLastRequestCurl()
    {
        return sprintf(
            "curl -X POST %s -H 'content-type: application/xml' -d '%s'",
            $this->getEndpoint(),
            $this->getLastRequest()
        );
    }

    public function getLastRequestXDebug()
    {
        return $this->getLastRequestCurl() . ' -b XDEBUG_SESSION=PHPSTORM';
    }

    protected function getEndpoint()
    {
        return Str::before($this->wsdl, '?wsdl');
    }
}
