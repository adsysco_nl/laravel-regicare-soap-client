<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\Fake;

trait Activiteit
{
    public function getActiviteiten()
    {
        return [
            1 => $this->getActiviteitItem(1),
            2 => $this->getActiviteitItem(2),
            3 => $this->getActiviteitItem(3),
            4 => $this->getActiviteitItem(4),
            5 => $this->getActiviteitItem(5),
            6 => $this->getActiviteitItem(6)
        ];
    }

    public function getActiviteitFilters()
    {
        return [
            'locatie' => [
                'label' => 'Locatie',
                'opties' => [
                    1 => 'AdSysCo'
                ]
            ],
            'interval' => [
                'label' => 'Frequentie',
                'opties' => [
                    1 => 'Wekelijks',
                    2 => 'Maandelijks'
                ]
            ],
            'dag' => [
                'label' => 'Dag',
                'opties' => []
            ],
            'seizoen' => [
                'label' => 'Seizoen',
                'opties' => []
            ],
            'werksoort' => [
                'label' => 'Werksoort',
                'opties' => []
            ],
            'leeftijdscategorie' => [
                'label' => 'Leeftijdscategorie',
                'opties' => []
            ],
            'activiteittype' => [
                'label' => 'Activiteittype',
                'opties' => [
                    80 => 'Computer'
                ]
            ],
            'groepering' => [
                'label' => 'Groepering',
                'opties' => []
            ],
            'verzorgingsgebied' => [
                'label' => 'Verzorgingsgebied',
                'opties' => []
            ],
            'opdrachtgever' => [
                'label' => 'Opdrachtgever',
                'opties' => []
            ],
            'contract' => [
                'label' => 'Contract',
                'opties' => []
            ],
            'filiaal' => [
                'label' => 'Filiaal',
                'opties' => []
            ]
        ];
    }

    public function getActiviteitBase($activiteitId)
    {
        return [
            'activiteitID' => $activiteitId,
            'code' => '100.000.000',
            'omschrijving' => $this->faker->randomElement(
                [
                    'Een presentatie over gezond worden, gezond zijn en gezond blijven',
                    $this->faker->sentence(6, true)
                ]
            ),
            'omschrijvingUitgebreid' => $this->faker->paragraphs(10, true),
            'trefwoorden' => 'foo',
            'bijzonderheden' => 'Niet gechikt voor mensen in een rolstoel',
            'url' => '',
            'startDatum' => $this->faker->randomElement([
                $this->faker->date('Y-m-d', '2019-05-07'),
                now()->addWeek()->format('Y-m-d'),
                now()->format('Y-m-d')
            ]),
            'eindDatum' => '2020-05-07',
            'startTijd' => '17:00',
            'eindTijd' => '20:00',
            'locatie' => [ 1 => 'AdSysCo'],
            'ruimte' => [1 => 'Boven'],
            'interval' => [2 => 'Elke week'],
            'dag' => [1 => 'Zondag']
        ];
    }

    public function getActiviteitItem($activiteitID)
    {
        return array_merge(
            $this->getActiviteitBase($activiteitID),
            [
                'inschrijvingen' => $this->faker->numberBetween(0, 10),
                'inschrijvingenMinimum' => 1,
                'inschrijvingenMaximum' => 10,
                'deelnemers' => 0,
                'bijeenkomsten' => 10,
                'bijdrage' => 1,
                'prijs' => number_format(12.95, 2, ',', '.'),
                'prijsExclBTW' => number_format(10.7, 2, ',', '.'),
                'inschrijven' => 1,
                'beschikbaar' => 1,
                'seizoen' => [],
                'werksoort' => [],
                'leeftijdscategorie' => [],
                'activiteittype' => [1 => 'Computer'],
                'groepering' => [],
                'verzorgingsgebied' => [],
                'opdrachtgever' => [],
                'contract' => [],
                'filiaal' => []
            ]
        );
    }

    public function getAcitviteitIngescheven($activiteitID)
    {
        return array_merge(
            $this->getActiviteitBase($activiteitID),
            [
                'inschrijvingen' => [[
                    'inschrijvingID' => 1,
                    'persoonID' => 126,
                    'opmerking' => 'Ik heb altijd al een keer willen leren punniken, dit wordt de dag van mijn leven!',
                    'van' => $this->faker->randomElement([
                        $this->faker->date('Y-m-d', '2019-05-07'),
                        now()->addWeek()->format('Y-m-d'),
                        now()->format('Y-m-d')
                    ]),
                    'tot' => '2020-05-07',
                    'status' => $this->faker->randomElement([10, 11, 20, 30])
                ]]
            ]
        );
    }

    public function activiteitInschrijven($activiteitId, $attributes)
    {
        return ['status' => 'OK', 'inschrijvingID' => 1];
    }

    public function activiteitUitschrijven($inschrijvingID)
    {
        return ['status' => 'OK'];
    }

    public function getActiviteitenIngeschreven()
    {
        if (!$this->loginKey()) {
            return [
                'error' => 'LOGIN_KEY_ONBEKEND', 'description' => ''
            ];
        }
        return [
            2 => $this->getAcitviteitIngescheven(2),
            3 => $this->getAcitviteitIngescheven(3),
            4 => $this->getAcitviteitIngescheven(4)
        ];
    }

    public function getActiviteitAantal()
    {
        return [
            'aantal' => 12
        ];
    }

    private function getBijeenkomst($bijeenkomstID)
    {
        return [
            'bijeenkomstID' => $bijeenkomstID,
            'activiteit' => [],
            'locatie' => [],
            'ruimte' => [],
            'omschrijving' => '',
            'datum' => $this->faker->randomElement([
                '2018-01-09',
                '2019-05-09',
                '2019-12-09',
                '2020-01-09',
            ]),
            'startTijd' => '',
            'eindTijd' => ''
        ];
    }

    public function getBijeenkomsten($activiteitID)
    {
        return [
            1 => $this->getBijeenkomst(1),
            2 => $this->getBijeenkomst(2),
            3 => $this->getBijeenkomst(3),
            4 => $this->getBijeenkomst(4)
        ];
    }
}
