<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\Fake;

use Adsysco\LaravelRegicareSoapClient\Models\Match\APIMatchFilters;
use Adsysco\LaravelRegicareSoapClient\Models\ProfielStatus;
use Adsysco\LaravelRegicareSoapClient\Models\Match\MatchScope;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\FakeSoapClient;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

trait RegiMatch
{
    protected $marktplaatsConfig;

    protected $marktplaatsProfiel = [];

    protected $praktischeHulpConfig;

    protected $praktischeHulpProfiel = [];

    protected $deletedAanbod = [];

    /**
     * @param  MatchScope  $scope
     * @return array|null
     */
    public function getMatchAanbodOverzicht(MatchScope $scope)
    {
        return [
            $this->makeAanbod($scope, FakeSoapClient::AANBOD_WITH_OWN_REACTIES),
            $this->makeAanbod($scope, FakeSoapClient::AANBOD_WITHOUT_REACTIES)
        ];
    }

    /**
     * @param  Collection  $scope
     * @return array|null
     */
    public function getMatchVraagOverzicht(Collection $scopes, APIMatchFilters $filters = null)
    {
        $requestFilters = $filters ? $filters->toArray() : ['limit' => 2];
        $items = collect([
            FakeSoapClient::getVraagWithOwnReacties(),
            FakeSoapClient::getVraagWithoutReacties()
        ]);

        return $items->take($requestFilters['limit']);
    }

    /**
     * @param  MatchScope  $scope
     * @param $aanbodId
     * @return mixed
     */
    public function getMatchAanbodInformatie(MatchScope $scope, $aanbodId)
    {
        if (in_array($aanbodId, $this->deletedAanbod)) {
            return [
                'error' => 'CHECK DE ECHTE API VOOR ERROR MELDING',
                'description' => ''
            ];
        }
        if (!(int)$aanbodId) {
            return ['error' => 'ONGELDIG AANBOD ID'];
        }

        switch ($aanbodId) {
            case 1:
                return $this->makeAanbod($scope, FakeSoapClient::AANBOD_WITH_OWN_REACTIES);
            case 2:
                return $this->makeAanbod($scope, FakeSoapClient::AANBOD_WITH_OTHER_REACTIES);
            default:
                return $this->makeAanbod($scope, FakeSoapClient::AANBOD_WITHOUT_REACTIES);
        }
    }

    /**
     * @param  MatchScope  $scope
     * @param $vraagId
     * @return mixed
     */
    public function getMatchVraagInformatie(MatchScope $scope, $vraagId)
    {
        if (!(int) $vraagId) {
            return ['error' => 'ONGELDIG VRAAG ID'];
        }
        if ((int) $scope->id === 2) {
            $vraag = FakeSoapClient::getVraagWithOwnReacties();
            $vraag['titel'] = 'Vraag uit Scope 2';
            return $vraag;
        }

        if ((int) $scope->id > 4) {
            return [
                'error' => 'ONGELDIG SCOPE ID',
                'description' => 'Er is een ongeldig werskoortID opgegeven'
            ];
        }

        switch ($vraagId) {
            case 1:
                return FakeSoapClient::getVraagWithOwnReacties();
            case 2:
                return FakeSoapClient::VRAAG_WITH_OTHER_REACTIES;
            case 3:
                return FakeSoapClient::getVraagWithoutReacties();
            default:
                return ['error' => 'VRAAG_ID_NIET_GEVONDEN', 'description' => 'De vraag id is dus niet gevonden'];
        }
    }

    public function getMatchVraagAantal(Collection $scopes, APIMatchFilters $filters = null)
    {
        return [
            'aantal' => 24
        ];
    }

    public function getMatchConfiguratie(MatchScope $scope)
    {
        switch ($scope->type) {
            case MatchScope::TYPE_MARKTPLAATS:
                $config = $this->marktplaatsConfig;
                $config['naam'] = $config['naam'] . ' ' . $scope->id;
                return $config;
                break;
            case MatchScope::TYPE_PRAKTISCHE_HULP:
                $config = $this->praktischeHulpConfig;
                $config['naam'] = $config['naam'] . ' ' . $scope->id;
                return $config;
                break;
        }
        return $this->marktplaatsConfig;
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function createMatchContactAanvraag(MatchScope $scope, $attributes)
    {
        return ['result' => 'OK'];
    }

    public function registerMatchProfiel(MatchScope $scope, $attributes)
    {
        return ['result' => 'OK'];
    }

    /**
     * @return array
     */
    public function getMatchProfielStatus(MatchScope $scope)
    {
        if (!Auth::check() || Auth::user()->isEmployee()) {
            return [
                'status' => ProfielStatus::NO_PROFILE,
            ];
        }
        if ((int) $scope->id === 4) {
            return [
                'status' => ProfielStatus::NO_PROFILE,
            ];
        }
        return [
            'status' => ProfielStatus::ACTIVE,
        ];
    }

    public function getMatchProfielGegevens(MatchScope $scope)
    {
        return $this->getProfileForScope($scope);
    }

    public function matchProfielAfmelden(MatchScope $scope)
    {
        return ['result' => 'OK'];
    }

    public function updateMatchProfielGegevens(MatchScope $scope, $attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->marktplaatsProfiel[$key] = $value;
        }
        return ['result' => 'OK'];
    }

    public function getMatchProfielFilters(MatchScope $scope)
    {
        return FakeSoapClient::MATCH_PROFIEL_FILTERS;
    }

    public function setMatchConfiguratieOptie($key, $value)
    {
        $this->marktplaatsConfig['opties'][$key] = $value;
    }

    public function getMatchAanbodFilter(MatchScope $scope)
    {
        return [
            'dienst' => [
                'naam' => 'Dienst',
                'opties' => [
                    15 => 'Computer',
                    16 => 'Dansen',
                    17 => 'Maatje',
                    18 => 'Creatief'
                ]
            ],
            'categorie' => [
                'naam' => 'Categorie',
                'opties' => [
                    1 => 'Categorie 1',
                    2 => 'Categorie 2'
                ]
            ],
            'regio' => [
                'naam' => 'Regio',
                'opties' => [
                    1 => 'Soest',
                    2 => 'Haarlem'
                ]
            ],
        ];
    }

    public function getMatchVraagFilter(Collection $scopes)
    {
        return [
            'dienst' => [
                'label' => 'Dienst',
                'opties' => [
                    15 => 'Computer',
                    16 => 'Dansen',
                    17 => 'Maatje',
                    18 => 'Creatief'
                ]
            ],
            'categorie' => [
                'label' => 'Categorie',
                'opties' => [
                    1 => 'Categorie 1',
                    2 => 'Categorie 2'
                ]
            ],
            'regio' => [
                'label' => 'Regio',
                'opties' => [
                    1 => 'Soest',
                    2 => 'Haarlem'
                ]
            ],
            'frequentie' => [
                'label' => 'Frequentie',
                'opties' => [
                    1 => '1x per dag',
                    2 => '1x per week',
                    3 => '1x per maand',
                    4 => '1x per jaar'
                ]
            ],
        ];
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function createVraagItem(MatchScope $scope, $attributes)
    {
        // TODO: Implement createVraagItem() method.
    }

    public function deleteVraagItem(MatchScope $scope, $vraagID)
    {
        if ($this->loginKey() !== FakeSoapClient::VALID_LOGINKEY) {
            return FakeSoapClient::API_ERROR_LOGIN_KEY_UNKOWN;
        }

        if ($vraagID !== FakeSoapClient::VRAAG_WITH_OTHER_REACTIES['vraagID']) {
            return [
                'error' => 'NOT ALLOWED TO DELETE SOMEONE ELSES VRAAG!',
                'description' => 'Mag niet'
            ];
        }

        return ['result' => 'OK'];
    }

    /**
     * @param  MatchScope  $scope
     * @param $attributes
     * @return mixed
     */
    public function createAanbodItem(MatchScope $scope, $attributes)
    {
        // TODO: Implement createAanbodItem() method.
    }

    /**
     * @param  MatchScope  $scope
     * @return array
     */
    public function getMatchAanbodAantal(MatchScope $scope)
    {
        return [
            'aantal' => 32
        ];
    }

    /**
     * @param  MatchScope  $scope
     * @param $aanbodID
     * @return mixed
     */
    public function deleteAanbodItem(MatchScope $scope, $aanbodID)
    {
        // Dont delete aanbod that you have not created yourFakeSoapClient.
        if ($aanbodID === FakeSoapClient::AANBOD_WITH_OWN_REACTIES['aanbodID']) {
            return ['error' => 'AANBOD_ID_ONGELDIG', 'description' => ''];
        }
        $this->deletedAanbod[] = $aanbodID;
    }

    /**
     * @return mixed
     */
    public function getMatchAanbodGeplaatst(MatchScope $scope)
    {
        $attributes = FakeSoapClient::AANBOD_WITHOUT_REACTIES;
        $attributes['titel'] .= sprintf(' s: %s i: %d', $scope->type, $scope->id);
        return [
            $this->makeAanbod($scope, $attributes)
        ];
    }

    public function makeAanbod(MatchScope $scope, $attributes = [])
    {
        $faker = \Faker\Factory::create();

        $reactionsKey = $scope->isMarktplaats() ? 'reacties' : 'match';
        $reactionIdKey = $scope->isMarktplaats() ? 'reactieID' : 'matchID';

        $aanbod = [
            'aanbodID' => $faker->randomNumber(),
            'titel' => $faker->sentence(2),
            'beschrijving' => $faker->sentence(20),
            'status' => 15,
            'van' => $faker->date(),
            $reactionsKey => [
                [
                    $reactionIdKey => 1,
                    'datum' => '2014-07-05',
                    'type' => 'type',
                    'naam' => 'Dave',
                    'opmerking' => 'Geplaatst door andere gebruiker',
                    'status' => 10
                ],
            ]
        ];
        return array_merge($aanbod, $attributes);
    }

    /**
     * @return mixed
     */
    public function getMatchAanbodGereageerd(MatchScope $scope)
    {
        return [
            [
                'aanbodID' => FakeSoapClient::AANBOD_WITH_OWN_REACTIES['aanbodID'],
                'titel' => FakeSoapClient::AANBOD_WITH_OWN_REACTIES['titel'],
                'beschrijving' => FakeSoapClient::AANBOD_WITH_OWN_REACTIES['beschrijving'],
                'reacties' => [
                    [
                        'reactieID' => 1,
                        'datum' => '2014-07-05',
                        'type' => 'Afwachtende reactie op aanbod',
                        'naam' => 'Dave',
                        'opmerking' => 'Geplaatst door huidige gebruiker',
                        'status' => 10
                    ],
                ]
            ]
        ];
    }

    public function getMatchAanbodReactieAvatar(MatchScope $scope, $reactieID)
    {
        return [
            'reactieID' => $reactieID,
            'type' => 1,
            'data' => FakeSoapClient::AFBEELDING
        ];
    }

    public function getMatchVraagReactieAvatar(MatchScope $scope, $reactieID)
    {
        return [
            'reactieID' => $reactieID,
            'type' => 1,
            'data' => FakeSoapClient::AFBEELDING
        ];
    }

    /**
     * @return mixed
     */
    public function getMatchVraagGeplaatst(MatchScope $scope)
    {
        $reactionsKey = $scope->isMarktplaats() ? 'reacties' : 'match';
        $reactionIdKey = $scope->isMarktplaats() ? 'reactieID' : 'matchID';

        $attributes = FakeSoapClient::VRAAG_WITH_OTHER_REACTIES + [
                $reactionsKey => [
                    [
                        $reactionIdKey => 1,
                        'datum' => '2014-07-05',
                        'type' => 'type',
                        'naam' => 'Pieter',
                        'opmerking' => 'Lorem',
                        'status' => 10
                    ],
                ]
            ];

        $attributes['titel'] .= sprintf(' s: %s i: %d', $scope->type, $scope->id);
        return [
            $attributes
        ];
    }

    /**
     * @return mixed
     */
    public function getMatchVraagGereageerd(MatchScope $scope)
    {
        return [
            [
                'vraagID' => FakeSoapClient::getVraagWithOwnReacties()['vraagID'],
                'titel' => 'Foo',
                'beschrijving' => 'Bar',
                'reacties' => [
                    [
                        'reactieID' => 1,
                        'datum' => '2014-07-05',
                        'type' => 'Afwachtende reactie op vraag',
                        'naam' => 'Dave',
                        'opmerking' => 'Lorem',
                        'status' => 10
                    ],
                ]
            ]
        ];
    }

    /**
     * @param  MatchScope  $scope
     * @param $vraagID
     * @param $attributes
     * @return array
     */
    public function createVraagReactie(MatchScope $scope, $vraagID, $attributes)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return array
     */
    public function cancelVraagReactie(MatchScope $scope, $reactieID)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param  MatchScope  $scope
     * @param $aanbodID
     * @param $attributes
     * @return array
     */
    public function createAanbodReactie(MatchScope $scope, $aanbodID, $attributes)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return array
     */
    public function cancelAanbodReactie(MatchScope $scope, $reactieID)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return mixed
     */
    public function acceptVraagReactie(MatchScope $scope, $reactieID)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return mixed
     */
    public function rejectVraagReactie(MatchScope $scope, $reactieID)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return mixed
     */
    public function acceptAanbodReactie(MatchScope $scope, $reactieID)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param  MatchScope  $scope
     * @param $reactieID
     * @return mixed
     */
    public function rejectAanbodReactie(MatchScope $scope, $reactieID)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param  MatchScope  $scope
     * @param $vraagId
     * @param $data
     * @return mixed
     */
    public function updateVraagItem(MatchScope $scope, $vraagId, $data)
    {
        // TODO: Implement updateVraagItem() method.
    }

    /**
     * @param  MatchScope  $scope
     * @param $aanbodId
     * @param $data
     * @return mixed
     */
    public function updateAanbodItem(MatchScope $scope, $aanbodId, $data)
    {
        // TODO: Implement updateAanbodItem() method.
    }

    /**
     * @param  MatchScope  $scope
     * @return array
     */
    private function getProfileForScope(MatchScope $scope): array
    {
        switch ($scope->type) {
            case MatchScope::TYPE_MARKTPLAATS:
                $profile = $this->marktplaatsProfiel;
                break;
            case MatchScope::TYPE_PRAKTISCHE_HULP:
                $profile = $this->praktischeHulpProfiel;
                break;
            default:
                $profile = $this->marktplaatsProfiel;
        }
        return $profile;
    }
}
