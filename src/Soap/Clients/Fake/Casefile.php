<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\Fake;

use Illuminate\Support\Facades\Auth;

trait Casefile
{
    protected $casefiles = [];

    protected $casefileContacts = [];

    public function getCasefiles()
    {
        return $this->casefiles->toArray();
    }

    public function getCasefileCount()
    {
        if (Auth::user()->gebruikersnaam !== self::VALID_USERNAME_CLIENT) {
            return ['aantal' => 0];
        }
        return [
            'aantal' => $this->casefiles->count()
        ];
    }

    public function getCasefile($caseID)
    {
        return $this->casefiles->first();
    }

    public function getCasefileContacts($caseID)
    {
        return $this->casefileContacts;
    }

    public function getCasefileMembers($caseID)
    {
        return [[
                    'naam' => $this->faker->name,
                    'type' => 'Informeel netwerk',
                    'rol' => 'Vader'
                ]];
    }

    public function getCasefileQuestionnaires($caseID)
    {
        return [
            [
                    'vragenlijstID' => 5,
                    'isCompleted' => 0,
                    'isAssigned' => 1,
                    'dateStarted' => '2019-10-28',
                    'name' => 'Intake enquete toegewezen'
            ],
            [
                    'vragenlijstID' => 4,
                    'isCompleted' => 1,
                    'isAssigned' => 1,
                    'dateStarted' => '2019-10-28',
                    'name' => 'Intake enquete toegewezen en compleet'
            ],
            [
                    'vragenlijstID' => 6,
                    'isCompleted' => 0,
                    'isAssigned' => 0,
                    'dateStarted' => '2019-10-28',
                    'name' => 'Intake enquete niet toegewezen'
            ],
        ];
    }

    public function getCasefileQuestionnaire($caseID, $questionnaireID)
    {
        return [
            'casusID' => $caseID,
            'vragenlijstID' => $questionnaireID,
            'status' => 1,
            'isAssigned' => 1,
            'vragen' => [
                [
                    'vraagID' => 1,
                    'type' => 4,
                    'title' => 'Heeft u eerder ondersteuning gehad rondom persoonlijke financieën?',
                    'isRequired' => 1,
                    'numOrder' => 1,
                    'items' => [
                        [
                            'itemID' => 1,
                            'type' => 1,
                            'label' => 'Ja',
                            'numOrder' => 1,
                        ],
                        [
                            'itemID' => 2,
                            'type' => 1,
                            'label' => 'Nee',
                            'numOrder' => 2,
                        ]
                    ]
                ],
                [
                    'vraagID' => 2,
                    'type' => 1,
                    'title' => 'Hoe problematisch ervaart u uw huidige situatie?',
                    'isRequired' => 1,
                    'numOrder' => 1,
                    'scale' => [
                        ['label' => 'helemaal niet'],
                        ['label' => 'niet'],
                        ['label' => 'een beetje'],
                        ['label' => 'erg'],
                        ['label' => 'heel erg'],
                    ],
                    'numScales' => [
                        1,2,3,4,5
                    ]
                ],
                [
                    'vraagID' => 3,
                    'type' => 5,
                    'title' => 'Overige opmerkingen',
                    'isRequired' => 0,
                    'numOrder' => 1,
                ]
            ]
        ];
    }

    public function getCasefileQuestionnaireResponse($caseID, $questionnaireID)
    {
        return [
            'casusID' => $caseID,
            'vragenlijstID' => $questionnaireID,
            'antwoorden' => [
                [
                    'vraagID' => 1,
                    'values' => [ 2 ]
                ]
            ]
        ];
    }

    public function saveCasefileQuestionnaireResponse($caseID, $questionnaireID, $response)
    {
        return ['result' => 'OK'];
    }

    public function submitCasefileQuestionnaireResponse($caseID, $questionnaireID, $response)
    {
        return ['result' => 'OK'];
    }

    private function createCasefiles($count = 1)
    {
        $casefiles = collect([]);
        for ($i = 0; $i < $count; $i++) {
            $casefiles->push($this->createCasefileData($i));
        }
        return $casefiles;
    }

    private function createCasefileData($id, $attributes = [])
    {
        $validData = collect([
            'casusID' => $id + 1,
            'status' => 1,
            'startDatum' => '21-03-2019',
            'eindDatum' => '25-12-2019',
            'casusBeheerder' => 'Mr Anderson'
        ]);

        return $validData->merge($attributes)->toArray();
    }

    private function createCasefileContacts($count = 1)
    {
        $contacts = collect([]);
        for ($i = 0; $i < $count; $i++) {
            $contacts->push($this->createCasefileContactData($i));
        }
        return $contacts;
    }

    private function createCasefileContactData($id, $attributes = [])
    {
        $validData = collect([
            'id' => $id,
            'datum' => '21-02-2018',
            'omschrijving' => $this->faker->sentence(),
            'samenvatting' => $this->faker->paragraph,
            'invoerder' => $this->faker->name,
            'categorieID' => $this->faker->randomElement([1,2,3]),
            'categorieLabel' => $this->faker->randomElement(['Met klant', 'Met collega', 'Met familie']),
            'wijzeID' => $this->faker->randomElement([1,2,3]),
            'wijzeLabel' => $this->faker->randomElement(['Face-to-face', 'Telefonisch', 'E-mail']),
            'status' => $this->faker->randomElement([1,2,3]),
            'tijdsduur' => 45
        ]);

        return $validData->merge($attributes)->toArray();
    }
}
