<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\Fake;

use Adsysco\LaravelRegicareSoapClient\Models\DeclaratieSoort;
use Adsysco\LaravelRegicareSoapClient\Soap\ApiResponse;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\FakeSoapClient;
use Illuminate\Support\Facades\Auth;

trait Declaratie
{
    protected $declaraties = [];

    /**
     * @param null $filter
     * @return mixed
     */
    public function getDeclaraties($filter = null)
    {
        if (Auth::user()->gebruikersnaam === self::VALID_USERNAME_CLIENT) {
            return (new ApiResponse(FakeSoapClient::API_ERROR_ACCESS_DENIED))->getData();
        }

        if ($this->loginKey() !== FakeSoapClient::VALID_LOGINKEY) {
            return (new ApiResponse(FakeSoapClient::API_ERROR_ACCESS_DENIED))->getData();
        }

        return $this->declaraties;
    }

    public function getDeclaratieItem($declaratieID)
    {
        if ($this->loginKey() !== FakeSoapClient::VALID_LOGINKEY) {
            return FakeSoapClient::API_ERROR_LOGIN_KEY_UNKOWN;
        }
        $declaratie = $this->createDeclaratieData(['declaratieID' => $declaratieID]);
        $declaratie['status'] = 10;
        return $declaratie;
    }

    public function createDeclaratieItem($attributes)
    {
        if ($this->loginKey() !== FakeSoapClient::VALID_LOGINKEY) {
            return FakeSoapClient::API_ERROR_LOGIN_KEY_UNKOWN;
        }

        $declaratie = $this->createDeclaratieData($attributes);
        $this->declaraties[] = $declaratie;
        return ['result' => 'OK', 'declaratieID' => $declaratie['declaratieID']];
    }

    public function updateDeclaratieItem($attributes)
    {
        return ['result' => 'OK'];
    }

    public function deleteDeclaratieItem($declaratieID)
    {
        // TODO: Implement deleteDeclaratieItem() method.
    }

    public function getBonForDeclaratie($declaratieID)
    {
        return [
            'declaratieID' => $declaratieID,
            'bon' => 'aGVsbG8gd29ybGQ=',
            'bestandsnaam' => 'fake_bon.png'
        ];
    }

    public function getDeclaratieSoort()
    {
        return [
            [
                'soortID' => 1,
                'type' => DeclaratieSoort::TYPE_KM,
                'omschrijving' => 'Kilometer (0.19)',
                'bedrag' => 0.19,
                'bon' => 1
            ],
            [
                'soortID' => 2,
                'type' => DeclaratieSoort::TYPE_OVERIG,
                'omschrijving' => 'Vrij invul vergoeding',
                'bon' => 0
            ],
            [
                'soortID' => 3,
                'type' => DeclaratieSoort::TYPE_VAST,
                'omschrijving' => 'Vaste vergoeding',
                'bedrag' => '100',
                'bon' => 0
            ],
        ];
    }

    public function getDeclaratieOrganisatie()
    {
        if ($this->loginKey() !== FakeSoapClient::VALID_LOGINKEY) {
            return FakeSoapClient::API_ERROR_LOGIN_KEY_UNKOWN;
        }
        return [
            [
                'organisatieID' => '1',
                'naam' => 'AdSysCo',
                'datumVan' => '2005-01-01',
                'datumTot' => '2020-01-01',
                'locaties' => []
            ],
            [
                'organisatieID' => '2',
                'naam' => 'ProFeel',
                'datumVan' => '2005-01-01',
                'datumTot' => '2020-01-01',
                'locaties' => []
            ]
        ];
    }

    public function getDeclaratieClient()
    {
        if ($this->loginKey() !== FakeSoapClient::VALID_LOGINKEY) {
            return FakeSoapClient::API_ERROR_LOGIN_KEY_UNKOWN;
        }
        return [
            [
                'clientID' => '1',
                'naam' => 'Client 1',
                'datumVan' => '2005-01-01',
                'datumTot' => '2030-01-01',
            ],
            [
                'clientID' => '2',
                'naam' => 'Client 2',
                'datumVan' => '2005-01-01',
                'datumTot' => '2030-01-01',
            ],
        ];
    }
}
