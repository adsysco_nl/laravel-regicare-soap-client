<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\Fake;

use Adsysco\LaravelRegicareSoapClient\Models\Vacature\VacaturebankProfielStatus;
use Adsysco\LaravelRegicareSoapClient\Models\Vacature\VacatureInvite;
use Adsysco\LaravelRegicareSoapClient\Models\Vacature\VacatureReaction;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\FakeSoapClient;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

trait Vacaturebank
{
    protected $vacaturebankConfig;

    protected $vacatures = [];

    /**
     * @return array|null
     */
    public function getVacatures($filters)
    {
        if (!$filters) {
            $filters = [
                'limit' => 5,
                'offset' => 0
            ];
        }

        if (isset($filters['limit']) && !isset($filters['offset'])) {
            $filters['offset'] = 0;
        }

        $filters = collect($filters);
        $this->vacatures->push(
            [
                'vacatureID' => 'fakeNumber',
                'titel' => 'Voorbeeld vacature',
                'tekst' => 'Voorbeeld tekst bij een vacature',
                'categorieNaam' => 'Ouderen (vanaf 65 jaar)',
                'dienstNaam' => 'Maatje',
                'gemarkeerd' => 1,
                'uitvoerders' => 3,
                'inschrijvingen' => 3,
                'organisatie' => 'AdSysCo',
                'organisatieStraat' => 'Industrieweg',
                'organisatieNummer' => '4d',
                'organisatiePlaats' => 'Heemstede',
                'locatie' => 'Loctie Zuid',
                'locatieStraat' => 'Herenweg',
                'locatieNummer' => '111',
                'locatiePlaats' => 'Alberquerque',
                'datumGepubliceerd' => '2015-06-11',
                'frequentieNaam' => 'Elke week',
                'van' => '2016-09-09',
                'eigenschappen' => [
                    0 => ['eigenschap' => 8, 'naam' => 'Reiskosten', 'waarde' => 1],
                    1 => ['eigenschap' => 2, 'naam' => 'VOG Gevraagd', 'waarde' => 1]
                ],
                'contactpersoon' => 'Ernst de Vries',
                'contactpersoonTelefoon' => '0612345678',
                'contactpersoonEmailadres' => 'edevries@adsysco.nl'
            ]
        );
        $vacatures = $this->vacatures;
        if ($filters->has('frequentie')) {
            $vacatures = $vacatures->whereIn('frequentie', $filters->get('frequentie'));
        }
        return $vacatures->slice($filters['offset'], $filters['limit'])->toArray();
    }

    public function getVacaturesGeplaatst($filters)
    {
        if (Auth::user()->can('access_user_links')) {
            return [
                'error' => SoapErrors::LOGIN_KEY_ONBEKEND,
                'description' => ''
            ];
        }

        $itemCount = 3;
        $vacatures = collect([]);

        $vacatures->push(
            [
                'vacatureID' => 1,
                'code' => 'A00001',
                'titel' => 'Voorbeeld vacature',
                'tekst' => 'Voorbeeld tekst bij vacature',
                'organisatieID' => 0,
                'locatieID' => 0,
                'uitvoerders' => 3,
                'contactpersoonID' => 0,
                'categorieNaam' => 'Ouderen (vanaf 65 jaar)',
                'dienstNaam' => 'Dieren vrijwilliger',
                'organisatieStraat' => 'Industrieweg',
                'organisatieNummer' => '4d',
                'organisatiePlaats' => 'Heemstede',
                'organisatie' => 'AdSysCo',
                'inschrijvingen' => [
                    1 => $this->makeVacatureInvite(1),
                    2 => $this->makeVacatureReaction(2),
                ],
                'status' => 20,
            ]
        );

        foreach (range(2, $itemCount) as $i) {
            $vacatures->push($this->getVacatureGeplaatstItem($i));
        }
        return $vacatures->toArray();
    }

    public function getVacatureCount($filters)
    {
        return ['aantal' => $this->vacatures->count()];
    }

    public function getVacatureCountWithSubscribed($filters)
    {
        return ['aantal' => $this->vacatures->count()];
    }

    public function getVacatureGeplaatstItem($id)
    {
        if (Auth::user() && Auth::user()->can('access_user_links')) {
            return [
                'error' => SoapErrors::LOGIN_KEY_ONBEKEND,
                'description' => ''
            ];
        }

        $vacature = $this->makeVacature($id);
        $geplaatstInfo = [
            'organisatieID' => 0,
            'locatieID' => 0,
            'contactpersoonID' => 0,
            'inschrijvingen' => [
                1 => $this->makeVacatureInvite(1, 10),
                2 => $this->makeVacatureInvite(2),
                3 => $this->makeVacatureReaction(3, 10),
                4 => $this->makeVacatureReaction(4),
                5 => $this->makeVacatureReaction(5, 11),
            ]
        ];
        return array_merge($vacature, $geplaatstInfo);
    }

    public function makeVacature($id)
    {
        $startDate = Carbon::create(2017, 3, 15, 0);
        return [
            'vacatureID' => $id,
            'code' => 'A00002' . $id,
            'titel' => 'Voorbeeld vacature ' . $id,
            'samenvatting' => $this->faker->sentence(42),
            'tekst' => $this->faker->paragraphs(10, true),
            'ervaringGewenst' => $this->faker->boolean(),
            'vervoerBenodigd' => $this->faker->boolean(),
            'van' => $startDate->format('Y-m-d'),
            'tot' => $startDate->addDays(21)->format('Y-m-d'),
            'uitvoerders' => 3,
            'inschrijvingen' => 1,
            'gemarkeerd' => $this->faker->randomElement([1, 0, 1, 0, 0]),
            'frequentie' => ($id % 2) + 1,
            'frequentieNaam' => 'Elke week',
            'publicatie' => $this->faker->boolean(),
            'publicatieVan' => $startDate->subWeek(1)->format('Y-m-d'),
            'publicatieTot' => $startDate->addWeek(2)->format('Y-m-d'),
            'publicatieControle' => $this->faker->boolean(),
            'categorie' => 1,
            'categorieNaam' => $this->faker->randomElement(['Ouderen (vanaf 65 jaar)', 'Jongeren', 'Iedereen']),
            'dienst' => $this->faker->randomElement([1, 2]),
            'dienstNaam' => $this->faker->randomElement(['Computer', 'Dansen', 'Maatje', 'Creatief']),
            'organisatieStraat' => 'Industrieweg',
            'organisatieNummer' => '4d',
            'organisatiePlaats' => 'Heemstede',
            'regioNaam' => 'Noord-Holland',
            'organisatie' => 'AdSysCo',
            'locatie' => 'Loctie Zuid',
            'locatieStraat' => 'Herenweg',
            'locatieNummer' => '111',
            'locatiePlaats' => 'Alkmaar',
            'status' => $this->faker->randomElement([10, 15, 20, 30]),
            'datumGepubliceerd' => $startDate->subWeek($this->faker->numberBetween(1, 52))->format('Y-m-d'),
            'eigenschappen' => [
                0 => ['eigenschap' => 8, 'naam' => 'Reiskosten', 'waarde' => $this->faker->randomElement([1, 2])],
                1 => ['eigenschap' => 2, 'naam' => 'VOG Gevraagd', 'waarde' => $this->faker->randomElement([1, 2])]
            ],
            'contactpersoon' => 'Ernst de Vries',
            'contactpersoonTelefoon' => '0612345678',
            'contactpersoonEmailadres' => 'edevries@adsysco.nl'
        ];
    }

    private function makeVacatureReaction($id, $status = null)
    {
        return [
            'inschrijvingID' => $id,
            'aanbiederID' => 0,
            'geslacht' => $this->faker->randomElement([1, 2]),
            'naam' => 'Manfred',
            'leeftijd' => $this->faker->numberBetween(1960, 1996),
            'plaats' => 'Heemstede',
            'opmerking' => 'Dit lijkt mij een erg leuke vacature! Aenean eu leo quam.' .
                'Pellentesque ornare sem lacinia quam venenatis vestibulum.' .
                'Cras justo odio, dapibus ac facilisis in, egestas eget quam.' .
                'Donec id elit non mi porta gravida at eget metus.',
            'van' => '2015-12-25',
            'tot' => '2018-12-25',
            'vrijveld1' => 1,
            'vrijveld1Naam' => '',
            'vrijveld2' => 1,
            'vrijveld2Naam' => '',
            'status' => $status ? $status : $this->faker->randomElement(array_keys(VacatureReaction::STATUSSES)),
            'score' => 1,
            'beoordeling' => '',
        ];
    }

    private function makeVacatureInvite($id, $status = null)
    {
        $invite = [
            'inschrijvingID' => $id,
            'aanbiederID' => $this->faker->numberBetween(1, 5),
            'geslacht' => $this->faker->randomElement([1, 2]),
            'naam' => 'Godfried',
            'leeftijd' => $this->faker->numberBetween(1960, 1996),
            'plaats' => 'Heemstede',
            'opmerking' => 'Jij lijkt ons een erg geschikte vrijwilliger! ' .
                'Aenean eu leo quam. Pellentesque ornare sem l' .
                'acinia quam venenatis vestibulum. Cras justo ' .
                'odio, dapibus ac facilisis in, egestas eget q' .
                'uam. Donec id elit non mi porta gravida at eget metus.',
            'van' => '2015-12-25',
            'tot' => '2018-12-25',
            'vrijveld1' => 1,
            'vrijveld1Naam' => '',
            'vrijveld2' => 1,
            'vrijveld2Naam' => '',
            'status' => $status ? $status : $this->faker->randomElement(array_keys(VacatureInvite::STATUSSES)),
            'score' => 1,
            'beoordeling' => ''
        ];

        $contact = [
            'naam' => 'Karel de Vries',
            'email' => 'karel@mailserver.com',
            'telefoon' => '0612345678',
        ];

        return $this->faker->boolean ? array_merge($invite, $contact) : $invite;
    }

    public function getVacatureTemplate()
    {
        return [
            'tekst' => '<P style="BORDER-BOTTOM-COLOR: ; BORDER-TOP-COLOR: ; BORDER-RIGHT-COLOR: ; ' .
                'BORDER-LEFT-COLOR: "><STRONG style="BORDER-BOTTOM-COLOR: ; BORDER-TOP-COLOR: ; BORDER-RIGHT-
                COLOR: ; ' .
                'BORDER-LEFT-COLOR: ">Wie zijn wij?</STRONG> </P><P style="BORDER-BOTTOM-COLOR: ; BORDER-TO
                P-COLOR: ; ' .
                'BORDER-RIGHT-COLOR: ; BORDER-LEFT-COLOR:">&nbsp;</P><P>' .
                '<STRONG style="BORDER-BOTTOM-COLOR: ; BORDER-TOP-COLOR: ; BORDER-RIGHT-COLOR: ; BORDER-LEFT-CO
                LOR: ' .
                '">Werkzaamheden</STRONG></P><P>&nbsp;</P><P style="BORDER-BOTTOM-COLOR: ; BORDER-TOP-C
                OLOR: ; ' .
                'BORDER-RIGHT-COLOR: ; BORDER-LEFT-COLOR: "><STRONG>Wij vragen</STRONG></P>' .
                '<P style="BORDER-BOTTOM-COLOR: ; BORDER-TOP-COLOR: ; BORDER-RIGHT-COLOR: ; BORDER-LEFT-C
                OLOR: ">&nbsp;' .
                '</P><P><STRONG>Wij bieden</STRONG></P><P>&nbsp;</P><P><STRONG>Tijdsbesteding </STRONG></P>' .
                '<P style="BORDER-BOTTOM-COLOR: ; BORDER-TOP-COLOR: ; BORDER-RIGHT-COLOR:' .
                '; BORDER-LEFT-COLOR: ">&nbsp;</P><P' .
                'style="BORDER-BOTTOM-COLOR: ; BORDER-TOP-COLOR:' .
                ' ; BORDER-RIGHT-COLOR: ; BORDER-LEFT-COLOR: "><STRONG>Interesse?' .
                '<BR></STRONG>Als&nbsp;u onderaan de vacature' .
                "op 'Inschrijven' klikt krijgen wij bericht van uw interesse. Er " .
                'zal dan zo spoedig mogelijk contact met u ' .
                'worden opgenomen. Hiervoor moet&nbsp;u wel ingelogd zijn. Bent u nog geen ' .
                'vrijwilliger bij <FONT color=#0000ff>' .
                '[organisatienaam]</FONT>? Klik dan <hyperlink invoegen> en stuur' .
                'ons een berichtje.&nbsp;Ook&nbsp;dan nemen we ' .
                'zo spoedig mogelijk contact met u op.</P>'
        ];
    }

    /**
     * @return array|null
     */
    public function getVacatureFilters()
    {
        return FakeSoapClient::VACATUREFILTERS;
    }

    public function createVacatureItem($data = [])
    {
        return ['status' => 'ok', 'vacatureID' => 1];
    }

    public function updateVacatureItem($data = [])
    {
        return ['status' => 'ok', 'vacatureID' => 1];
    }

    public function deleteVacatureItem($vacatureID)
    {
        return ['status' => 'ok'];
    }

    /**
     * @param $vacatureId
     * @return mixed
     */
    public function getVacatureItem($vacatureId)
    {
        return $this->makeVacature($vacatureId);
    }

    public function registerVacaturebankProfiel($attributes)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param $vacatureId
     * @return mixed
     */
    public function sendReaction($vacatureId, $data)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param $vacatureId
     * @return mixed
     */
    public function cancelReaction($vacatureId)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param $vacatureId
     * @return mixed
     */
    public function acceptReaction($vacatureID)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param $vacatureId
     * @return mixed
     */
    public function rejectReaction($vacatureID)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function createVacatureContactAanvraag($attributes)
    {
        return ['result' => 'OK'];
    }

    /**
     * @return array
     */
    public function getVacaturebankProfielStatus()
    {
        if (!Auth::check() || Auth::user()->isEmployee()) {
            return [
                'status' => VacaturebankProfielStatus::NO_PROFILE,
            ];
        }
        return [
            'status' => VacaturebankProfielStatus::ACTIVE,
        ];
    }

    /**
     * @return array
     */
    public function getVacaturebankProfielGegevens()
    {
        return $this->vacaturebankProfiel;
    }

    public function vacaturebankProfielAfmelden()
    {
        return ['result' => 'OK'];
    }

    public function updateVacaturebankProfielGegevens($attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->vacaturebankProfiel[$key] = $value;
        }
        return ['result' => 'OK'];
    }

    public function getVacaturebankProfielFilters()
    {
        return FakeSoapClient::VACATUREBANK_PROFIEL_FILTERS;
    }

    public function getVacaturebankAanbiederFilters()
    {
        return FakeSoapClient::VACATUREBANK_PROFIEL_FILTERS;
    }

    public function getReactions()
    {
        if (!$this->loginKey()) {
            return [
                'error' => 'LOGIN_KEY_ONBEKEND', 'description' => ''
            ];
        }
        $vacatures = collect($this->getVacatures([]));
        $vacatures = $vacatures->map(function ($vacature) {
            $vacature = collect($vacature)->put('inschrijvingen', collect([]));
            $vacature['inschrijvingen'][10] = $this->makeVacatureReaction(10, 10);
            $statusses = $this->faker->randomElements(array_keys(VacatureReaction::STATUSSES), 2)
                + [VacatureReaction::STATUS_SUCCESVOL_BEMIDDELD];
            foreach ($statusses as $status) {
                $vacature['inschrijvingen'][$status] = $this->makeVacatureReaction(
                    $vacature['vacatureID'] . $status,
                    $status
                );
            }
            return $vacature->toArray();
        });
        return $vacatures;
    }

    public function getVacatureAantal()
    {
        return ['aantal' => 16];
    }

    /**
     * @param $volunteerID
     * @return array|null
     */
    public function getVolunteer($volunteerID)
    {
        return [
            'code' => $this->faker->numerify('A#####'),
            'aanbiederID' => $volunteerID,
            'geslacht' => $this->faker->randomElement([1, 2]),
            'leeftijd' => $this->faker->numberBetween(1960, 1996),
            'plaats' => 'Heemstede',
            'motivatie' => 'Nu ik gepensioneerd ben wil ik graag mijn vrije tijd nuttig gebruiken.',
            'presentatie' => 'Ik ben Simon, voormalig politieagent ' .
                'en trotse vader van twee kinderen. Mi' .
                'jn hobby\'s zijn joggen, hardlopen en rennen.',
            'vog' => true,
            'ehbo' => true,
            'dienst' => $this->faker->randomElements(
                [15, 16, 17, 18, 19, 20],
                $this->faker->numberBetween(0, 6)
            ),
            'categorie' => [1],
            'regio' => [$this->faker->randomElement([1, 2, 3])],
            'beschikbaarMaVan' => '09:00',
            'beschikbaarMaTot' => '14:00',
            'beschikbaarDiVan' => '09:00',
            'beschikbaarDiTot' => '14:00',
            'beschikbaarWoVan' => null,
            'beschikbaarWoTot' => null,
            'beschikbaarDoVan' => '09:00',
            'beschikbaarDoTot' => '14:00',
            'beschikbaarVrVan' => '09:00',
            'beschikbaarVrTot' => '14:00',
            'beschikbaarZaVan' => null,
            'beschikbaarZaTot' => null,
            'beschikbaarZoVan' => '09:00',
            'beschikbaarZoTot' => '14:00'
        ];
    }

    /**
     * @return array|null
     */
    public function getVolunteers()
    {
        return array_merge(collect(range(1, 27))->map(function ($id) {
            return $this->getVolunteer($id);
        })->toArray());
    }

    /**
     * @return array|null
     */
    public function getInvites($filter = [])
    {
        if (!$this->loginKey()) {
            return [
                'error' => 'LOGIN_KEY_ONBEKEND',
                'description' => ''
            ];
        }
        $vacatures = collect($this->getVacatures([], 2));
        $vacatures = $vacatures->map(function ($vacature) {
            $vacature = collect($vacature)->put('inschrijvingen', collect([]));
            if (rand(0, 1) > 0) {
                $vacature['inschrijvingen'][8] = $this->makeVacatureInvite($vacature['vacatureID'] . 8, 10);
                foreach ($this->faker->randomElements(array_keys(VacatureInvite::STATUSSES), 2) as $status) {
                    $vacature['inschrijvingen'][$status] = $this->makeVacatureInvite(
                        $vacature['vacatureID'] . $status,
                        $status
                    );
                }
            }
            return $vacature->toArray();
        });
        return $vacatures;
    }

    /**
     * @param $vacatureID
     * @param $aanbiederID
     * @return array|null
     */
    public function inviteVolunteer($vacatureID, $volunteerID, $attributes)
    {
        return [
            'result' => 'OK',
            'inschrijvingID' => 1
        ];
    }

    /**
     * @param $inviteID
     * @return array|null
     */
    public function cancelInvite($inviteID)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param $inviteID
     * @return array|null
     */
    public function acceptInvite($inviteID)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param $inviteID
     * @return array|null
     */
    public function completeInvite($inviteID)
    {
        return ['result' => 'OK'];
    }

    /**
     * @param $inviteID
     * @return array|null
     */
    public function rejectInvite($inviteID)
    {
        return ['result' => 'OK'];
    }

    public function getVacaturebankConfiguratie()
    {
        return $this->vacaturebankConfig;
    }

    public function setVacaturebankConfiguratieOptie($key, $value)
    {
        $this->vacaturebankConfig['opties'][$key] = $value;
    }

    public function getInvitationForVolunteer($id)
    {
        return [
            'vacature' => $this->makeVacature($id),
            'tekst' => 'Lorem ipsum in dolor sit amet bla',
            'status' => $this->faker->randomElement(['open', 'afgewezen', 'ingeschreven']),
        ];
    }

    public function getInvitationsForVolunteer()
    {
        return array_merge(collect(range(1, 20))->map(function ($id) {
            return $this->getInvitationForVolunteer($id);
        })->toArray());
    }

    public function declineInvitation($invitationID)
    {
        return ['result' => 'OK'];
    }

    public function getInvitationForEmployee($id)
    {
        return [
            'vacature' => $this->makeVacature($this->faker->randomElement([1, 2, 3, 4])),
            'vrijwilliger' => $this->getVolunteer($id),
            'tekst' => 'Lorem ipsum in dolor sit amet bla',
            'status' => $this->faker->randomElement(['open', 'afgewezen', 'ingeschreven']),
        ];
    }

    public function getInvitationsForEmployee()
    {
        return array_merge(collect(range(1, 20))->map(function ($id) {
            return $this->getInvitationForEmployee($id);
        })->toArray());
    }
}
