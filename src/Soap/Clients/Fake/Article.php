<?php


namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients\Fake;

use Adsysco\LaravelRegicareSoapClient\Soap\Clients\FakeSoapClient;
use Carbon\Carbon;

trait Article
{
    /**
     * @param $filters
     * @return mixed
     */
    public function getPublicArticles($filter = null)
    {
        return [
            FakeSoapClient::ARTICLE,
            $this->getArticle(1),
            $this->getArticle(2),
        ];
    }

    /**
     * @param $filters
     * @return mixed
     */
    public function getAllArticles($filter = null)
    {
        return [
            FakeSoapClient::ARTICLE,
            $this->getArticle(1),
            $this->getArticle(2),
            $this->getArticle(3),
            $this->getArticle(4),
        ];
    }

    /**
     * @param  $nieuwsId
     * @return mixed
     */
    public function createNieuwsItem($nieuwsId)
    {
        return [
        'nieuwsID' => $nieuwsId,
        'categorie' => $this->faker->randomElement(['succesverhalen','algemeen','evenement', '']),
        'titel' => $this->faker->randomElement(
            [
                'Bewegen houdt ouderen jong',
                'Nieuwe openingstijden',
                $this->faker->sentence(12, true)
            ]
        ),
        'samenvatting' => $this->faker->paragraphs(1, true),
        'tekst' => $this->faker->paragraphs(10, true),
        'datum' => $this->faker->date('Y-m-d', Carbon::create(FakeSoapClient::ARTICLE['datum'])),
        'afbeelding' => 0
            ];
    }

    /**
     * @param  $nieuwsId
     * @return mixed
     */
    public function createNieuwsAfbeelding($nieuwsId)
    {
        $nieuws = $this->createNieuwsItem($nieuwsId);
        // @codingStandardsIgnoreStart
        $nieuws['afbeelding']='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wgARCACAAIADASIAAhEBAxEB/8QAHAABAAICAwEAAAAAAAAAAAAAAAYHAwUBBAgC/8QAGQEBAQADAQAAAAAAAAAAAAAAAAQBAgMF/9oADAMBAAIQAxAAAAH08AAAAafDjFStky1T8VygAAAAK0svzfw77SZZrD579Tucc1ShkAAANLhHKU7en870vS2/x5PR84M4AAARvYeeZ6JZEOdVDdkmGjsXfS1B6fmAAAARCmLnoyC3p9iMTGevR3ZQPqqqWQC2EAAACl4PeEI86/bYLUrTppE77p24umgUzAAAAR7pbDXSd5hDJloeumul8akucB10AAAAjsPm+Ph0+schiO2PiY0vc+HI7aAAf//EACMQAAICAgIBBQEBAAAAAAAAAAIEAwUBBgAwERITFBUxIDT/2gAIAQEAAQUC6n25lDp3ZW0+24zENUox6ENay0qfbt7uRzrFWzYM2OHaKJNkHVezYH/sGtFW9il/efnZs94CcZwQHmhhwvUdlraQVavyCmZEFcvAHth1WV8jW8sdxsccmZiNsxzgdWhnsr7qs2vhpmzDxdZhYzBRlMwOyHUSlja6tnP0V7zkyxjVhMc4RHmaDMM+mp58dWyswxqW6oo4QEvNimVZBNKWZ6BUk6jqvkLJrdrs4jPTdfBRfcoCzO5VRTbN17AGfRd0w/FjjxFHuLGMSRPZl3zrvBxlS6xj3ebFXA/xZSEdi67r/LcmPufvLXPjkOc/f9d2BErsFtWq1lLZxXNbbngOQEf3vXb06d2rTUiFCpzY5XYkxqtyuhx58f1//8QAIxEAAQQBAwUBAQAAAAAAAAAAAQACAxEgBBIiITFBUWETgf/aAAgBAwEBPwHAdTSfExkf3PTtt1+k+ciT4jiBZoKOPYKRNm8YIf06lNYyPspnHactORtW03yWp6Ny0pY2rUT3Sz2tRfEH1lH4/qiJDuKffnIP20mHlaeQe2H/xAAkEQABBAEDBQADAAAAAAAAAAABAAIDESAEEiETIjFBYVFxgf/aAAgBAgEBPwHA8BMmc6T5nOabSbAHM+oYk0LT5OoU0UKxnm6fATnuk8lQgbhlqR3rjyFpxbstTucT8UzBHBS09d1fnKXjd/FPXTNqL3+8nQ7ib9qVpLCAomlt3h//xAA3EAABAgQEAwUGBAcAAAAAAAACAQMABBESBRMhQSIwMhQxQlFhIzNSY3GhEIGRsSAkYnOSweH/2gAIAQEABj8C5UraAK247Y6pL0pSEWYpnho7QbUu501nGYArJIpB3p9IRcyZxAW0CwGjq7l7LD8jNTJONmubLZnVb5c6WkW3KEtz/wDj3Vh0ZqaUMpoKtiOglt9oScFkXxZX4oZm2+l0L05s0RzKIxM3NNDu4idNv3hCoaXlSjnUlI1SO7mJhUsVZycFUH5YU1NYkWEmJR+UPjAXNHOraJYEbsqGYo+pa81X3SG5dGxXxLDuJnRT9lMOPN69o01EYZwwGFKX9425dx8W/wDyBb+FKcsgPMedBNWmQuWHVayZZoFHu4zWsPPzMzMuzAcHtA0AiXT9IAXsWAHpN61SAOEK7QarKttSkhXweOuiV+/LN26irRsdK8S6QU08/Mzepv3XWJpwWU/OBaw1sGZcwq6t6EaLbv8ASHTTEfaCrWc6QLxfSGxl5cHpVwCM1ssueTcodlyfuqHF5GenEPpry65xtcXUA1VN4FZh8wFx8zBoGUoYeRQM22/V/wB6TImiUT46rtByJyzbAuorzz7a1toq/b0hoAmv5WzLSzeqd5eXfD+IuNEK0SWaUl1yx5YS/aMqYmHLJdd7u/8AasPvI4arVMxv5nmMI0ok7emWKb67QuFYgY3ZWcnp6KUNS0yPvUEuHo9IlmHERDtuOi11XXlyE48FMPk2VUD2v3ioXqmqIS7wOJzQVec1ar4RhoqJQwtovijD5G1OFlpCproiaxonLCYTZFGMJZRLTVLC/f8A3AtCmgJbEoxZXqc+sZaBVLKAtPDl8xK7HGHejv4S9y046RMPWJUWkt5iIm5xIJ/VfFUiV/vJEynyR5gkHeJxIzjrpOIi0RWkvqtNUhjEGm3GxcTpNNUpDKr8VYdqnXQvypzOxz2bl1rwOKMdhw9DRpSU+M7tfwApHCXJ9y/pAkSn6w1Pu4iOEnWisZOtE9axr3/x/wD/xAAlEAEAAgICAwACAgMBAAAAAAABESEAMUFRMGFxgaGR4RAgsfD/2gAIAQEAAT8h8VzdjjLRO7DJn1uo7wfBJ8yi4KRMR+c4Dr6PjKX195q9ZopLgXv3TnrzHcnxIChjPQs21WMIXGTF4wElUO8E01kQwVf4nNdG+LryheyWB4NJBKXUGY5y+OhghrDHta4xAoCOxwAgACgOPISgzDyPVoY7wwaSqUumFNAkxOS3NymHXfNvl3YX39scr1koTMli57QetBeBrraWilaiYPnc3l3zHP4R414t1gZ1MUT7wSMWMT8yFOINYQMiQ9Xqrcr07wEsn9mOJLIuoicdIciM7efuYlEeMZC9pLPGudz+MnOFRDX2ZG87xKAGcZ+ZNoCa7xN+m6iQTtdTMayut2MdtNMhzpe8GMJiAEmxlxjfrxuYlS1uNfcCHtxAyTsQQjTPDc4kKYR7s261eycdn3E063D37tXWHfQxumdmgbFh9dYiSByraxoljXXjR8ZCTGLQ9H+R3i0E5ESkubC7fcYEmLyJWMW20Yt6AbMOo5wS1ETzmru7VHODZHPGUe9IM67/AD424tu3QpjvX4MkWqfqlZ3yTMYR0sRmfT9f0fcB62btLQRM6a6xpmy5J3Y8QN+8/B68Y0bPPsOb6zA/955FeCfgRlySLojhENduAywuJsn9+SJkwP6ckWiD9GO3Gi72jcTgCj1RZIH/AA8i1UkX8ce15B+Bgx4G8l+x4riH8teSOuM291jtHu4UK6L7cjrgY7qE962UmDtCEZ7DCTIAl5f9fIMBW5ufabPTjNewVL23rXH+DsyWpzbLXyciyHPJemLUemOcrTGFoQL/AL//2gAMAwEAAgADAAAAEPPPPOffPPPPPPhvfPPPOXvPPPPIPK/PPPPIT4PPPPPHWPvPPPPBCvPPPPPFnF/PPP/EACURAQABBAECBgMAAAAAAAAAAAERACAhMZFBYVFxgcHR4aGx8P/aAAgBAwEBPxCwQeKnGZ39N+9+n0QMCMR6RSFktQtjWvebTMutuXJHanpOTmitOY4+/wBXGj1zSIFlz90kpe3vcnPsXifj811dWeIpk7xyvxc1T/N0FVDQCdhdGgZJoArVOh0EWf/EACQRAQABAwMEAgMAAAAAAAAAAAERACAxIVGBQWGRobHBcdHw/9oACAECAQE/ELFI1rYjHnH1f+cUR1jdzNBCG0GWCpd4igMdLUjla2ap5J193InZClhkCgljpP1cEjEPcftrt5HmaEl2OAuYv6yUUBk0+SkJDd83BNaAO+lZmYo3cV82f//EACEQAAECBAcAAAAAAAAAAAAAAAERMBBAUPAAICFRYYGx/9oACAEBAAE/EG8qcvP+befb+v8AdxAZ3/pcx/8A4zCa/wCH0z/X/g0z8zz77/Hqf/8Al7chGfDv/f8AHytr/wDc/wDO/wDYJAxq/rfid9zb/wDunB7fB5PZ3/dOux/8deff++bLy1ZKZZ/kYmd//U3/AIX79Gwlz/8A6Snrpfo74HctL/4bd696uL9q3/EUKf7/AOC+v3//AKk5fNnXPf6O/wDRlcJ//wDlLvL5vvkkVj9foG5af2/taOaf+a2lV0k76d5Nv/8A/v71t/5nnPc//wAgP/8A2yd23DyWX781OXv/AFg9y7r3/J9dHX//AMkFrn/nq9fIgZrm0dfEm1O/506Y1LdNn7z8l6/6Qf8A/N3zX/6cXzs9KPzSwSz5b8LLJL//AM//AP8A/d1AgPZ942gaL/8A4t6MeneJzgtht1/3/wD/AP/Z';
        return $nieuws;
    }

    /**
     * @param  $articleId
     * @return mixed
     */
    public function getArticle($articleId)
    {
        if ($articleId == 0) {
            return FakeSoapClient::ARTICLE;
        }
        if ($articleId % 2 == 0) {
            return $this->createNieuwsItem($articleId);
        }
        return $this->createNieuwsAfbeelding($articleId);
    }
}
