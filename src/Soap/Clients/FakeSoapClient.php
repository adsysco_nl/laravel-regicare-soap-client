<?php

namespace Adsysco\LaravelRegicareSoapClient\Soap\Clients;

use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Fake\Casefile;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts\CasefileInterface;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts\ActiviteitInterface;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts\DeclaratieInterface;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts\MatchInterface;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts\ArticleInterface;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Contracts\VacaturebankInterface;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Fake\Activiteit;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Fake\Declaratie;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Fake\Article;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Fake\RegiMatch;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\Fake\Vacaturebank;
use Adsysco\LaravelRegicareSoapClient\Auth\ApiUser;
use Adsysco\LaravelRegicareSoapClient\Soap\SoapErrors;
use Illuminate\Support\Facades\Auth;
use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\SoapClient;
use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\RegiService;

/**
 * Class FakeSoapClient.
 *
 * @codeCoverageIgnore
 */
class FakeSoapClient implements
    SoapClient,
    RegiService,
    ActiviteitInterface,
    CasefileInterface,
    DeclaratieInterface,
    MatchInterface,
    ArticleInterface,
    VacaturebankInterface
{
    use Activiteit;
    use Casefile;
    use Declaratie;
    use RegiMatch;
    use Article;
    use Vacaturebank;

    public $isExternal = false;
    public $siteUrl = 'http://regiextranet.localhost';
    public $vacatureFilters = FakeSoapClient::VACATUREFILTERS;

    const VALID_LOGINKEY = 'login-key';

    const VALID_USERNAME = 'valid-user@adsysco.nl';

    const VALID_USERNAME_CLIENT = 'valid-client@adsysco.nl';

    const VALID_USERNAME_EMPLOYEE = 'valid-employee@adsysco.nl';

    const VALID_PASSWORD = 'valid-password';

    const INVALID_ZIPCODE = '1000AA';

    const INVALID_STREETNUMBER = '1';

    const AFBEELDING = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAFoEvQfAAAABGdBTUEAALGPC/xhBQAAAA1JREFUCB1j+M9w5j8ABpgCy/'
            . 'Z3+aMAAAAASUVORK5CYII=';

    const USER_DATA = [
        'voorletters' => 'D.B.',
        'roepnaam' => 'Dave',
        'tussenvoegsel' => null,
        'achternaam' => 'Developer',
        'geboortedatum' => '1982-02-24',
        'geslacht' => '1',
        'emailadres' => 'valid-user@adsysco.nl',
        'gebruikersnaam' => 'valid-user@adsysco.nl',
        'telefoonMobiel' => '0612345678',
        'telefoonVast' => null,
        'postcode' => '2102 LH',
        'nummer' => '4',
        'toevoeging' => 'd',
        'straat' => 'Industrieweg',
        'plaats' => 'Heemstede',
        'iban' => 'NL91ABNA0417164300',
        'land' => 'NL'
    ];

    const MATCH_PROFIEL_FILTERS = [
        'redenAanmelding' => [
            'label' => 'Reden aanmelding',
            'opties' => [
                1 => 'Maatschappelijke betrokkenheid',
                2 => 'Persoonlijke interesse'
            ]
        ],
        'bekendVia' => [
            'label' => 'Bekend via',
            'opties' => [
                1 => 'Via via',
                2 => 'Krant artikel'
            ]
        ],
        'vrijveld1' => [
            'label' => 'Vrijveld 1',
            'opties' => [
                1 => 'Optie 1',
                2 => 'Optie 2'
            ]
        ],
        'vrijveld2' => [
            'label' => 'Vrijveld 2',
            'opties' => [
                1 => 'Optie 1',
                2 => 'Optie 2'
            ]
        ],
        'vrijveld3' => [
            'label' => 'Vrijveld 3',
            'opties' => [
                1 => 'Optie 1',
                2 => 'Optie 2'
            ]
        ],
        'vrijveld4' => [
            'label' => 'Vrijveld 4',
            'opties' => [
                1 => 'Optie 1',
                2 => 'Optie 2'
            ]
        ],
        'ervaring' => [
            'label' => 'Ervaring',
            'nodes' => [
                1 => 'Veld 1',
                2 => 'Veld 2'
            ],
            'opties' => [
                1 => [1 => 'Optie 1', 2 => 'Optie 2'],
                2 => [3 => 'Optie 3', 4 => 'Optie 4']
            ]
        ],
        'interesse' => [
            'label' => 'Interesse',
            'nodes' => [
                1 => 'Interesse 1',
                2 => 'Heb je verder nog interesses?'
            ],
            'opties' => [
                1 => [1 => 'Interesse 1', 2 => 'Interesse 2'],
                2 => [3 => 'Interesse 3', 4 => 'Interesse 4']
            ]
        ],
        'eigenschap' => [
            'label' => 'Eigenschappen',
            'nodes' => [
                1 => 'Veld 1',
                2 => 'Veld 2'
            ],
            'opties' => [
                1 => [8 => 'Reiskosten'],
                2 => [
                        2 => 'VOG Gevraagd',
                        6 => 'VOG verplicht (kosten worden vergoed)'
                    ]
            ]
        ],
    ];

    const MARKTPLAATS_PROFIEL = [
        'redenAanmelding' => 2,
        'bekendVia' => 1,
        'vrijveld1' => 1,
        'vrijveld2' => 2,
        'vrijveld3' => 'Misschien wel ja',
        'vrijveld4' => true
    ];

    const PRAKTISCHE_HULP_PROFIEL = [
        'redenAanmelding' => 1,
        'bekendVia' => 2,
        'vrijveld1' => 2,
        'vrijveld2' => 1,
        'vrijveld3' => 'Misschien wel ja',
        'vrijveld4' => false,
        'motivatie' => 'Ik help graag',
        'situatie' => 'Ik hou van gezelligheid',
        'ervaring' => [1],
        'interesse' => [1],
        'eigenschap' => [1],
        'beschikbaarMaVan' => '11:00',
        'beschikbaarMaTot' => '13:00',
        'beschikbaarDiVan' => null,
        'beschikbaarDiTot' => null,
        'beschikbaarWoVan' => null,
        'beschikbaarWoTot' => null,
        'beschikbaarDoVan' => null,
        'beschikbaarDoTot' => null,
        'beschikbaarVrVan' => null,
        'beschikbaarVrTot' => null,
        'beschikbaarZaVan' => null,
        'beschikbaarZaTot' => null,
        'beschikbaarZoVan' => null,
        'beschikbaarZoTot' => null
    ];

    const AANBOD_WITH_OWN_REACTIES = [
        'aanbodID' => 1,
        'code' => 'A1',
        'titel' => 'Fietsmaatje in Soest',
        'beschrijving' => 'Aanbod waar de gebruiker zelf op heeft gereageerd',
        'dienst' => 15,
        'naam' => 'Pieter Crucq',
        'plaats' => 'Soest',
        'postcode' => '3762 TJ',
        'bemiddelaarNaam' => 'Mr. Bemiddelaar',
        'bemiddelaarGeslacht' => 1,
        'bemiddelaarLeeftijd' => 30
    ];
    const AANBOD_WITH_OTHER_REACTIES = [
        'aanbodID' => 2,
        'code' => 'A2',
        'titel' => 'Aanbod 2',
        'beschrijving' => 'Aanbod van uzelf waar iemand anders op heeft gereageerd',
        'dienst' => 15,
        'naam' => 'Pieter Crucq',
        'plaats' => 'Soest',
        'postcode' => '3762 TJ',
        'bemiddelaarNaam' => 'Mr. Bemiddelaar',
        'bemiddelaarGeslacht' => 1,
        'bemiddelaarLeeftijd' => 30,
    ];
    const AANBOD_WITHOUT_REACTIES = [
        'aanbodID' => 3,
        'code' => 'A3',
        'titel' => 'Aanbod 3',
        'beschrijving' => 'Beschrijving van een aanbod',
        'dienst' => 15,
        'naam' => 'Pieter Crucq',
        'plaats' => 'Soest',
        'bemiddelaarNaam' => 'Mr. Bemiddelaar',
        'bemiddelaarGeslacht' => 1,
        'bemiddelaarLeeftijd' => 30
    ];

    const ARTICLE = [
        'nieuwsID' => 0,
        'categorie' => 'sport',
        'titel' => 'Foo Bar',
        'samenvatting' => 'Lorem ipsum in dolar',
        'tekst' => 'Lorem ipsum in dolor sit amet, Lorem ipsum in dolor sit amet Lorem ipsum in dolor sit amet',
        'datum' => '2020-05-07',
        'afbeelding' => 0
    ];


    public static function getVraagWithOwnReacties()
    {
        return [
            'vraagID' => 1,
            'code' => 'V1',
            'werksoort' => config('services.regiApi.werksoortIdPraktischeHulp'),
            'titel' => 'Ik zoek een chauffeur voor elke vrijdag van Soest naar Heemstede',
            'beschrijving' => 'Beschrijving van een vraag',
            'status' => 10,
            'dienst' => 15,
            'naam' => 'Pieter Crucq',
            'plaats' => 'Soest',
            'bemiddelaarNaam' => '',
            'bemiddelaarGeslacht' => '',
            'bemiddelaarLeeftijd' => ''
        ];
    }

    public static function getVraagWithoutReacties()
    {
        return [
            'vraagID' => 3,
            'code' => 'V3',
            'werksoort' => config('services.regiApi.werksoortIdPrikbord'),
            'titel' => 'Een vraag met een heel erg lange titel, nog veel langer dan die met de
            titel \'Ik zoek een chauffeur voor elke vrijdag van Soest naar Heemstede\'',
            'beschrijving' => 'Beschrijving van een vraag',
            'status' => 10,
            'dienst' => 16,
            'naam' => 'Pieter Crucq',
            'plaats' => 'Nederhorst den Berg',
            'bemiddelaarNaam' => '',
            'bemiddelaarGeslacht' => '',
            'bemiddelaarLeeftijd' => ''
        ];
    }

    const VRAAG_WITH_OTHER_REACTIES = [
        'vraagID' => 2,
        'code' => 'V2',
        'titel' => 'Vraag 2',
        'beschrijving' => 'GEPLAATST AANBOD!',
        'status' => 10,
        'dienst' => 15,
        'naam' => 'Pieter Crucq',
        'plaats' => 'Soest',
        'bemiddelaarNaam' => 'Mr. Bemiddelaar',
        'bemiddelaarGeslacht' => 1,
        'bemiddelaarLeeftijd' => 30
    ];

    const API_ERROR_LOGIN_KEY_UNKOWN = [
        'error' => SoapErrors::LOGIN_KEY_ONBEKEND,
        'description' => 'De opgegeven loginKey is onbekend'
    ];
    const API_ERROR_ACCESS_DENIED = [
        'error' => SoapErrors::LOGIN_KEY_ONBEVOEGD,
        'description' => 'De opgegeven loginKey is onbekend'
    ];

    const VACATUREBANK_PROFIEL = [
        'redenAanmelding' => 2,
        'bekendVia' => 1,
        'dienst' => [15],
        'categorie' => [1],
        'regio' => [1],
        'ervaring' => [1],
        'interesse' => [1],
        'eigenschap' => [1],
        'presentatie'=> 'Lorem ipsum in do dolor sit amet ipsum in dolor sit amet ipsum in dolor sit amet ipsum',
        'beschikbaarMaVan' => '11:00',
        'beschikbaarMaTot' => '13:00',
        'beschikbaarDiVan' => null,
        'beschikbaarDiTot' => null,
        'beschikbaarWoVan' => null,
        'beschikbaarWoTot' => null,
        'beschikbaarDoVan' => null,
        'beschikbaarDoTot' => null,
        'beschikbaarVrVan' => null,
        'beschikbaarVrTot' => null,
        'beschikbaarZaVan' => null,
        'beschikbaarZaTot' => null,
        'beschikbaarZoVan' => null,
        'beschikbaarZoTot' => null,
        'ehbo' => 1,
        'vog' => 0,
        'openbaar' => 1,
        'vrijveld1' => 1,
        'vrijveld2' => 2,
        'vrijveld3' => 'Misschien',
        'vrijveld4' => 0,
    ];

    const VACATUREFILTERS = [
        'no-options' => ['label' => 'Filter without options', 'opties' => []],
        'dienst' => ['label' => 'Dienst', 'opties' => ['dienst A', 'dienst B']],
        'categorie' => ['label' => 'Categorie', 'opties' => ['a' => 'b', 'b' => 'c']],
        'eigenschap' => ['label' => 'Eigenschap', 'opties' => [
            8 => 'Reiskosten',
            2 => 'VOG Gevraagd',
            6 => 'VOG verplicht (kosten worden vergoed)'
        ]],
        'frequentie' => ['label' => 'Frequentie', 'opties' => [1 => 'Wekelijks', 2 => 'Maandelijks']]
    ];

    protected $faker;

    private $userData = [];

    const VACATUREBANK_PROFIEL_FILTERS = [
        'redenAanmelding' => [
            'label' => 'Reden aanmelding',
            'opties' => [
                1 => 'Maatschappelijke betrokkenheid',
                2 => 'Persoonlijke interesse'
            ]
        ],
        'bekendVia' => [
            'label' => 'Bekend via',
            'opties' => [
                1 => 'Via via',
                2 => 'Krant artikel'
            ]
        ],
        'dienst' => [
            'label' => 'Dienst',
            'opties' => [
                15 => 'Computer',
                16 => 'Dansen',
                17 => 'Maatje',
                18 => 'Creatief',
                19 => 'Thuiszorg',
                20 => 'Foo'
            ]
        ],
        'categorie' => [
            'label' => 'Categorie',
            'opties' => [
                1 => 'Test Categorie',
                2 => 'Nog een categorie'
            ]
        ],
        'regio' => [
            'label' => 'Regio',
            'opties' => [
                1 => 'Nieuwegein',
                2 => 'Amsterdam',
                3 => 'Den Haag'
            ]
        ],
        'ervaring' => [
            'label' => 'Ervaring',
            'nodes' => [
                1 => 'Veld 1',
                2 => 'Veld 2'
            ],
            'opties' => [
                1 => [1 => 'Optie 1', 2 => 'Optie 2'],
                2 => [3 => 'Optie 3', 4 => 'Optie 4']
            ]
        ],
        'interesse' => [
            'label' => 'Interesse',
            'nodes' => [
                1 => 'Interesse 1',
                2 => 'Heb je verder nog interesses?'
            ],
            'opties' => [
                1 => [1 => 'Interesse 1', 2 => 'Interesse 2'],
                2 => [3 => 'Interesse 3', 4 => 'Interesse 4']
            ]
        ],
        'eigenschap' => [
            'label' => 'Eigenschappen',
            'nodes' => [
                1 => 'Veld 1',
                2 => 'Veld 2'
            ],
            'opties' => [
                1 => [8 => 'Reiskosten'],
                2 => [
                        2 => 'VOG Gevraagd',
                        6 => 'VOG verplicht (kosten worden vergoed)'
                    ]
            ]
        ],
        'vrijveld1' => [
            'label' => 'Vrijveld 1',
            'opties' => [
                1 => 'Optie 1',
                2 => 'Optie 2'
            ]
        ],
        'vrijveld2' => [
            'label' => 'Vrijveld 2',
            'opties' => [
                1 => 'Optie 1',
                2 => 'Optie 2'
            ]
        ],
        'vrijveld3' => [
            'label' => 'Vrijveld 3',
            'opties' => [
                1 => 'Optie 1',
                2 => 'Optie 2'
            ]
        ],
        'vrijveld4' => [
            'label' => 'Vrijveld 4',
            'opties' => [
                1 => 'Optie 1',
                2 => 'Optie 2'
            ]
        ]
    ];

    const VACATUREBANK_CONFIGURATIE = [
        'naam' => 'vacaturebank',
        'organisatie' => 'test',
        'type' => 'vacaturebank',
        'opties' => [
            'inschakelen_vacatures' => 1,
            'inschakelen_vrijwilligers' => 1,
            'inschakelen_contact' => 1,
            'inschakelen_aanmelden' => 1,
            'inschakelen_inloggen' => 1,
            'inschakelen_profiel' => 1,
            'inschakelen_profiel_uitgebreid' => 1,
            'inschakelen_profiel_voorkeuren' => 1,
            'inschakelen_profiel_beschikbaarheid' => 1,
            'inschakelen_inschrijvingen' => 1,
            'inschakelen_afmelden' => 1,
            'tonen_filter_dienst' => 1,
            'tonen_filter_regio' => 1,
            'tonen_filter_categorie' => 1,
            'tonen_vacature_organisatie' => 1,
            'tonen_vacature_organisatie_adres' => 1,
            'tonen_vacature_contactpersoon' => 1,
            'tonen_vacature_contactpersoon_telefoon' => 1,
            'tonen_vacature_contactpersoon_emailadres' => 1,
            'tonen_vacature_vervoer_benodigd' => 1,
            'tonen_vacature_ervaring_gewenst' => 1,
            'tonen_aanmelding_reden_aanmelding' => 1,
            'tonen_aanmelding_bekend_via' => 1,
            'tonen_aanmelding_opleiding' => 1,
            'tonen_aanmelding_onderwijsinstelling' => 1,
            'tonen_aanmelding_onderwijsmedewerker' => 1,
            'tonen_aanmelding_vrijveld1' => 1,
            'tonen_aanmelding_vrijveld2' => 1,
            'tonen_aanmelding_vrijveld3' => 1,
            'tonen_aanmelding_vrijveld4' => 1,
            'tonen_profiel_iban' => 1,
            'tonen_profiel_opleiding' => 1,
            'tonen_profiel_onderwijsinstelling' => 0,
            'tonen_profiel_onderwijsmedewerker' => 0,
            'tonen_profiel_presentatie' => 1,
            'tonen_profiel_vog' => 1,
            'tonen_profiel_ehbo' => 1,
            'tonen_profiel_ervaring' => 1,
            'tonen_profiel_eigenschap' => 1,
            'tonen_profiel_interesse' => 1,
            'tonen_profiel_vrijveld1' => 1,
            'tonen_profiel_vrijveld2' => 1,
            'tonen_profiel_vrijveld3' => 1,
            'tonen_profiel_vrijveld4' => 1,
            'tonen_inschrijving_vrijveld1' => 1,
            'tonen_inschrijving_vrijveld2' => 1,
            'verplichten_telefoon_vast' => 1,
            'verplichten_telefoon_mobiel' => 1,
            'verplichten_geboortedatum' => 1,
            'verplichten_opleiding' => 1,
            'verplichten_onderwijsinstelling' => 1,
            'verplichten_onderwijsmedewerker' => 1,
            'verplichten_vrijveld1' => 1,
            'verplichten_vrijveld2' => 1,
            'verplichten_vrijveld3' => 1,
            'verplichten_vrijveld4' => 1,
            'verplichten_inschrijving_vrijveld1' => 1,
            'verplichten_inschrijving_vrijveld2' => 1,
            'goedkeuren_vacature' => 1,
            'goedkeuren_aanmelding' => 0,
            'goedkeuren_inschrijving' => 1,
            'verplichten_login' => 1,
            'informeel_taalgebruik' => 1,
            'samenvatting_vacatures' => 1,
            'max_vacatures' => 500,
            'max_inschrijvingen' => 0
        ]
    ];

    const MARKTPLAATS_CONFIGURATIE = [
        'naam' => 'Marktplaats',
        'organisatie' => 'test',
        'beschrijving' => 'Kom nu bij de marktplaats en al je dromen komen uit',
        'opties' => [
            'verplichten_login' => 0,
            'inschakelen_informeel_taalgebruik' => 1,
            'inschakelen_contact' => 1,
            'inschakelen_aanmelden' => 1,
            'inschakelen_inloggen' => 1,
            'inschakelen_profiel' => 1,
            'inschakelen_afmelden' => 1,
            'tonen_filter_dienst' => 1,
            'tonen_filter_regio' => 1,
            'tonen_filter_categorie' => 1,
            'verplichten_telefoon_vast' => 0,
            'verplichten_telefoon_mobiel' => 1,
            'verplichten_geboortedatum' => 1,
            'tonen_aanmelding_reden_aanmelding' => 1,
            'tonen_aanmelding_bekend_via' => 1,
            'tonen_profiel_vrijveld1' => 1,
            'tonen_aanmelding_vrijveld1' => 1,
            'verplichten_vrijveld1' => 1,
            'tonen_profiel_vrijveld2' => 1,
            'tonen_aanmelding_vrijveld2' => 1,
            'verplichten_vrijveld2' => 1,
            'tonen_profiel_vrijveld3' => 1,
            'tonen_aanmelding_vrijveld3' => 1,
            'verplichten_vrijveld3' => 1,
            'tonen_profiel_vrijveld4' => 1,
            'tonen_aanmelding_vrijveld4' => 1,
            'verplichten_vrijveld4' => 1
        ]
    ];

    const PRAKTISCHE_HULP_CONFIGURATIE = [
        'naam' => 'Schrijfmaatje',
        'organisatie' => 'test',
        'beschrijving' => 'Schrijven is leuk, doe je mee?',
        'opties' => [
            'verplichten_login' => 0,
            'inschakelen_informeel_taalgebruik' => 1,
            'inschakelen_contact' => 1,
            'inschakelen_aanmelden' => 1,
            'inschakelen_inloggen' => 1,
            'inschakelen_profiel' => 1,
            'inschakelen_afmelden' => 1,
            'tonen_filter_dienst' => 1,
            'tonen_filter_regio' => 1,
            'tonen_filter_categorie' => 1,
            'verplichten_telefoon_vast' => 0,
            'verplichten_telefoon_mobiel' => 1,
            'verplichten_geboortedatum' => 1,
            'tonen_aanmelding_reden_aanmelding' => 1,
            'tonen_aanmelding_bekend_via' => 1,
            'tonen_profiel_vrager_vrijveld1' => 1,
            'tonen_profiel_vrijwilliger_vrijveld1' => 1,
            'tonen_aanmelding_vrijveld1' => 1,
            'verplichten_vrijveld1' => 1,
            'tonen_profiel_vrager_vrijveld2' => 1,
            'tonen_profiel_vrijwilliger_vrijveld2' => 0,
            'tonen_aanmelding_vrijveld2' => 1,
            'verplichten_vrijveld2' => 1,
            'tonen_profiel_vrager_vrijveld3' => 0,
            'tonen_profiel_vrijwilliger_vrijveld3' => 1,
            'tonen_aanmelding_vrijveld3' => 1,
            'verplichten_vrijveld3' => 1,
            'tonen_profiel_vrager_vrijveld4' => 1,
            'tonen_profiel_vrijwilliger_vrijveld4' => 1,
            'tonen_aanmelding_vrijveld4' => 1,
            'verplichten_vrijveld4' => 1,
            'tonen_profiel_vrager_ervaring' => 1,
            'tonen_profiel_vrijwilliger_ervaring' => 1,
            'tonen_profiel_vrager_eigenschap' => 1,
            'tonen_profiel_vrijwilliger_eigenschap' => 1,
            'tonen_profiel_vrager_interesse' => 1,
            'tonen_profiel_vrijwilliger_interesse' => 1,
            'inschakelen_profiel_vrager_beschikbaarheid' => 1, // @todo - not implemented in API
            'inschakelen_profiel_vrijwilliger_beschikbaarheid' => 1, // @todo - not implemented in API
        ]
    ];

    private $vacaturebankProfiel = [];

    private $validLogins;


    public function __construct($isExternal = null)
    {
        if (isset($isExternal)) {
            $this->isExternal = $isExternal;
        }
        $this->faker = \Faker\Factory::create('nl_NL');
        $this->userData = self::USER_DATA;
        $this->vacaturebankProfiel = self::VACATUREBANK_PROFIEL;
        $this->marktplaatsProfiel = self::MARKTPLAATS_PROFIEL;
        $this->praktischeHulpProfiel = self::PRAKTISCHE_HULP_PROFIEL;
        $this->vacatures = $this->createVacatures(12);
        $this->declaraties = $this->createDeclaraties(4);
        $this->declaraties->prepend($this->createDeclaratieData(['bon' => 1]));
        $this->declaraties->prepend($this->createDeclaratieData(['soortID' => 2]));
        $this->casefiles = $this->createCasefiles(1);
        $this->casefileContacts = collect([
            $this->createCasefileContactData(3, [
                'omschrijving' => 'Start schuldhulpverlening',
                'datum' => '28-03-2019',
                'samenvatting' => 'We zijn gestart met de schuldhulpverlening en hebben besproken wat de doelen zijn.'
            ]),
            $this->createCasefileContactData(2, [
                'omschrijving' => 'Vraag verheldering',
                'datum' => '23-03-2019',
                'samenvatting' => 'Formulieren doorgesproken en de financiele situatie extra doorgesproken met Meneer.'
            ]),
            $this->createCasefileContactData(1, [
                'omschrijving' => 'Initiele aanmelding',
                'datum' => '21-03-2019',
                'samenvatting' => 'Eerste contact gehad en alle benodigde formulieren mee gegeven.'
            ]),
        ]);

        $this->validLogins = collect([
            [
                'username' => self::VALID_USERNAME,
                'password' => self::VALID_PASSWORD
            ],
            [
                'username' => self::VALID_USERNAME_CLIENT,
                'password' => self::VALID_PASSWORD
            ],
            [
                'username' => self::VALID_USERNAME_EMPLOYEE,
                'password' => self::VALID_PASSWORD
            ],
        ]);
        $this->marktplaatsConfig = self::MARKTPLAATS_CONFIGURATIE;
        $this->praktischeHulpConfig = self::PRAKTISCHE_HULP_CONFIGURATIE;
        $this->vacaturebankConfig = self::VACATUREBANK_CONFIGURATIE;
    }

    /**
     * @return mixed
     */
    protected function loginKey()
    {
        if (!Auth::check()) {
            return null;
        }
        return Auth::user()->getAuthIdentifier();
    }

    private function createDeclaraties($count = 1)
    {
        $declaraties = collect([]);
        for ($i = 0; $i < $count; $i++) {
            $declaraties->push($this->createDeclaratieData());
        }
        return $declaraties;
    }

    private function createDeclaratieData($attributes = [])
    {
        $id = random_int(0, 1);

        $validData = collect([
            'declaratieID' => count($this->declaraties) + 1,
            'soortID' => 1,
            'locatieID' => '',
            'locatieNaam' => '',
            'organisatieID' => $id + 1,
            'organisatieNaam' => $id ? 'ProFeel' : 'AdSysCo',
            'clientID' => $id + 1,
            'clientNaam' => 'Client ' . ($id + 1),
            'nummer' => random_int(1, 100),
            'datum' => '2017-02-03',
            'aantal' => 12.34,
            'bedrag' => 56.78,
            'opmerking' => 'Lorem',
            'status' => $this->faker->randomElement([10, 15, 20, 30]),
            'datumGoedgekeurd' => null,
            'datumUitbetaald' => null,
            'bon' => rand(0, 1)
        ]);

        return $validData->merge($attributes)->toArray();
    }

    private function createVacatures($count = 10)
    {
        $vacatures = collect([]);
        for ($i = 1; $i < $count + 1; $i++) {
            $vacatures->push($this->makeVacature($i));
        }
        return $vacatures;
    }

    /**
     * @return array
     */
    public function status()
    {
        // TODO: Implement status() method.
    }

    /**
     * @param $username
     * @param $password
     * @return array
     */
    public function login($username, $password)
    {
        if (!$this->validateUser($username, $password)) {
            return [
                'error' => SoapErrors::LOGIN_KEY_ONBEKEND,
                'description' => ''
            ];
        }
        return [
            'loginKey' => self::VALID_LOGINKEY,
            'rol' => ($username === self::VALID_USERNAME_EMPLOYEE) ? ApiUser::ROLE_MEDEWERKER : ApiUser::ROLE_USER,
        ];
    }

    /**
     * @return array
     */
    public function logout()
    {
        return ['status' => 'OK'];
    }

    private function validateUser($username, $password)
    {
        if ($this->validLogins->isEmpty()) {
            return false;
        }

        return collect($this->validLogins->first(function ($loginInfo) use ($username, $password) {
            return $loginInfo['username'] === $username && $loginInfo['password'] === $password;
        }))->isNotEmpty();
    }

    /**
     * @param array $userData
     * @return array
     */
    public function register(array $userData)
    {
        $this->validLogins->push([
            'username' => $userData['emailadres'],
            'password' => $userData['wachtwoord']
        ]);

        return ['status' => 'OK'];
    }

    /**
     * @param $loginKey
     * @return array
     */
    public function profile($loginKey)
    {
        if ($loginKey !== self::VALID_LOGINKEY) {
            return self::API_ERROR_LOGIN_KEY_UNKOWN;
        }

        return $this->userData;
    }

    /**
     * @param $attributes
     * @return mixed
     */
    public function updateProfile($attributes)
    {
        if ($this->loginKey() !== self::VALID_LOGINKEY) {
            return self::API_ERROR_LOGIN_KEY_UNKOWN;
        }

        $this->userData = array_merge($this->userData, $attributes);
        return $this->userData;
    }

    public function createProfielContactAanvraag($attributes)
    {
        return ['result' => 'OK'];
    }

    public function getLinkedOrganizations()
    {
        return [
            [
                'organisatieID' => 1,
                'naam' => 'Organisation with Location',
                'nummer' => $this->faker->randomNumber(),
                'toevoeging' => '2 hoog',
                'postcode' => '2012CA',
                'plaats' => 'Haarlem',
            ],
            [
                'organisatieID' => 2,
                'naam' => 'Organisation without Locations',
                'nummer' => $this->faker->randomNumber(),
                'toevoeging' => '2 hoog',
                'postcode' => '2012CA',
                'plaats' => 'Haarlem',
            ]
        ];
    }

    public function getLinkedLocations($organizationID)
    {
        $locations = collect(
            [
                [
                    'organisatieID' => 1,
                    'locatieID' => 1,
                    'naam' => $this->faker->sentence(2),
                    'straat' => $this->faker->sentence(2),
                    'nummer' => $this->faker->randomNumber(),
                    'toevoeging' => '',
                    'postcode' => '2011AA',
                    'plaats' => 'Haarlem'
                ]
            ]
        );

        return $locations->where('organisatieID', $organizationID)->all();
    }

    public function getLinkedEmployees($organizationID)
    {
        return [
            [
                'organisatieID' => 1,
                'medewerkerID' => 1,
                'roepnaam' => $this->faker->firstName(),
                'voorletters' => $this->faker->randomLetter(1),
                'tussenvoegsel' => '',
                'achternaam' => $this->faker->lastName,
                'postcode' => '2011AA',
                'plaats' => $this->faker->city(),
            ],
            [
                'organisatieID' => 1,
                'medewerkerID' => 2,
                'roepnaam' => 'Dave',
                'voorletters' => 'D',
                'tussenvoegsel' => '',
                'achternaam' => 'Developer',
                'postcode' => '2011AA',
                'plaats' => $this->faker->city(),
            ]
        ];
    }

    /**
     * Get the avatar data for a loginKey.
     *
     * @return array
     */
    public function avatar()
    {
        if ($this->loginKey() !== self::VALID_LOGINKEY) {
            return [
                'error' => 'Invalid login-key',
                'description' => ''
            ];
        }

        return [
            'naam' => '',
            'data' => '',
            'type' => ''
        ];
    }

    /**
     * Update avatar data for a loginKey.
     *
     * @param $attributes
     * @return array
     */
    public function updateAvatar($attributes)
    {
        if ($this->loginKey() !== self::VALID_LOGINKEY) {
            return [
                'error' => 'Invalid login-key',
                'description' => ''
            ];
        }

        return [
            'result' => 'OK'
        ];
    }

    public function verify($credentials)
    {
        return true;
    }

    /**
     * @param string $username
     * @return array
     */
    public function checkUsername($username)
    {
        return ['result' => 'OK'];
    }

    public function resetPassword($username)
    {
        return ['result' => 'OK'];
    }

    public function getAddressFromPostal($postalCode, $streetNumber)
    {
        // These values should not return a postal code.
        if ($postalCode === self::INVALID_ZIPCODE && $streetNumber === self::INVALID_STREETNUMBER) {
            return [
                'error' => 'ADRES_ONBEKEND',
                'description' => 'De opgegeven combinatie van postcode en nummer is onbekend.'
            ];
        }

        return [
            'straat' => 'Industrieweg',
            'plaats' => 'Heemstede'
        ];
    }

    /**
     * @return mixed
     */
    public function profielAfmelden()
    {
        $this->validLogins = collect([]);
        return ['result' => ['OK']];
    }

    public function getLastRequestCurl()
    {
        return '---- FakeSoapClient ----';
    }

    public function enableDebug()
    {
        return '---- FakeSoapClient ----';
    }
}
