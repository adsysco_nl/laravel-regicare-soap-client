<?php

namespace Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients;

interface RegiService
{
    /**
     * @return array
     */
    public function status();

    /**
     * @param $username
     * @param $password
     * @return array
     */
    public function login($username, $password);

    /**
     * @param array $userData
     * @return array
     */
    public function register(array $userData);

    /**
     * @param $loginKey
     * @return array
     */
    public function profile($loginKey);

    /**
     * @param $attributes
     * @return mixed
     */
    public function updateProfile($attributes);

    /**
     * @param $attributes
     * @return mixed
     */
    public function createProfielContactAanvraag($attributes);

    /**
     * @return mixed
     */
    public function getLinkedOrganizations();

    /**
     * @param $organizationID
     * @return mixed
     */
    public function getLinkedLocations($organizationID);

    /**
     * @param $organizationID
     * @return mixed
     */
    public function getLinkedEmployees($organizationID);

    /**
     * @return mixed
     */
    public function avatar();

    /**
     * @param $credentials
     * @return mixed
     */
    public function verify($credentials);

    /**
     * @param $postalCode
     * @param $streetNumber
     * @return mixed
     */
    public function getAddressFromPostal($postalCode, $streetNumber);

    /**
     * @param string $username
     * @return array
     */
    public function checkUsername($username);

    /**
     * @param $username
     * @return mixed
     */
    public function resetPassword($username);

    /**
     * @return mixed
     */
    public function profielAfmelden();
}
