<?php

namespace Adsysco\LaravelRegicareSoapClient\Traits;

use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\SoapClient;

trait HasLoginKey
{
    public function getLoginKey()
    {
        return $this->webSessions->last()->hash;
    }
}
