<?php

namespace Adsysco\LaravelRegicareSoapClient\Providers;

use Adsysco\LaravelRegicareSoapClient\Listeners\LoginWithApi;
use Illuminate\Auth\Events\Attempting;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{

    protected $listen = [
        Attempting::class => [
            LoginWithApi::class,
        ],
    ];
}
