<?php

namespace Adsysco\LaravelRegicareSoapClient;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Adsysco\LaravelRegicareSoapClient\Skeleton\SkeletonClass
 */
class LaravelRegicareSoapClientFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'laravel-regicare-soap-client';
    }
}
