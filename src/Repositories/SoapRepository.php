<?php

namespace Adsysco\LaravelRegicareSoapClient\Repositories;

use Adsysco\LaravelRegicareSoapClient\Soap\Clients\ApiSoapClient;
use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\SoapClient;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Adsysco\LaravelRegicareSoapClient\Soap\Clients\FakeSoapClientWrapper;

class SoapRepository
{
    /**
     * @var SoapClient
     */
    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function all()
    {
        if ($this->app->environment('testing')) {
            return $this->getFakeSoapClients();
        }

        $soapClients = collect([]);
        $apiKeys = config('auth.providers.users.apiKeys');
        $wsdls = config('auth.providers.users.wsdls');
        $websites = config('auth.providers.users.websites');
        $werksoortIds = config('services.regiApi.werksoordtIdsVacaturebank');
        $vacatureFilters = config('client.vacature_filters');
        $wrapper = $this->app->make(SoapWrapper::class);

        foreach ($apiKeys as $key => $apiKey) {
            $soapClients->push(new ApiSoapClient(
                $apiKey,
                $wsdls[$key],
                (bool) $key,
                $key,
                $websites[$key],
                $werksoortIds[$key],
                $vacatureFilters[$key] ?? [],
                $wrapper
            ));
        }

        return $soapClients;
    }

    public function getInternal()
    {
        $soapClients = $this->all();
        return $soapClients->filter(function ($soap) {
            return !$soap->isExternal;
        });
    }

    public function getExternal()
    {
        $soapClients = $this->all();
        return $soapClients->filter(function ($soap) {
            return $soap->isExternal;
        });
    }

    private function getFakeSoapClients()
    {
        return collect([])
            ->push(new FakeSoapClientWrapper(false))
            ->push(new FakeSoapClientWrapper(true))
            ->push(new FakeSoapClientWrapper(true));
    }
}
