<?php

namespace Adsysco\LaravelRegicareSoapClient\Listeners;

use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\SoapClient;
use Illuminate\Auth\Events\Attempting;

class LoginWithApi
{

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(Attempting $event)
    {
        // @todo: handle failure
        $result = resolve(SoapClient::class)->login($event->credentials['username'], $event->credentials['password']);
        session(['regicare.loginKey' => $result['loginKey']]);
    }
}
