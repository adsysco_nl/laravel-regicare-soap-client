<?php

namespace Adsysco\LaravelRegicareSoapClient\Models;

use Illuminate\Http\Request;

class APIFilters
{

    /**
     * @var Request
     */
    protected $request;
    protected $without;
    protected $keys;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->without = collect([]);

        $this->setFilters();

        $this->limit = (int) $this->request->get('limit', 10);
        $this->offset = (int) $this->request->get('offset', 0);
    }

    public function without($keys)
    {
        $this->without = $this->without->merge($keys);
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return collect($this->keys)
            ->diff($this->without)
            ->filter(function ($key) {
                return $this->$key !== null;
            })
            ->mapWithKeys(function ($key) {
                return [$key => $this->$key];
            })->toArray();
    }

    /**
     * @param $keys
     * @return \Illuminate\Support\Collection
     */
    public function only($keys)
    {
        return collect($keys)
            ->mapWithKeys(function ($key) {
                return [$key => $this->$key];
            });
    }

    protected function getFilterParameter($parameter)
    {
        if (!$this->request->filled($parameter)) {
            return;
        }
        return collect(explode(',', $this->request->get($parameter)))
            ->map(function ($item) {
                return (int) $item;
            })
            ->toArray();
    }
}
