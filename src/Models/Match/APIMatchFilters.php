<?php

namespace Adsysco\LaravelRegicareSoapClient\Models\Match;

use Adsysco\LaravelRegicareSoapClient\Models\APIFilters;

class APIMatchFilters extends APIFilters
{
    protected $frequentie;
    protected $dienst;
    protected $categorie;
    protected $regio;
    protected $tag;

    protected $keys = [
        'frequentie',
        'dienst',
        'regio',
        'categorie',
        'offset',
        'limit',
        'tag',
    ];

    protected function setFilters()
    {
        $this->frequentie = $this->getFilterParameter('frequentie');
        $this->dienst = $this->getFilterParameter('dienst');
        $this->categorie = $this->getFilterParameter('categorie');
        $this->regio = $this->getFilterParameter('regio');
        $this->tag = $this->request->get('tag');
    }

    protected function getFilterParameter($parameter)
    {
        if (!$this->request->filled($parameter)) {
            return;
        }
        return collect($this->request->get($parameter))
            ->map(function ($item) {
                return (int) $item;
            })
            ->toArray();
    }
}
