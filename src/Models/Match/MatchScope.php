<?php

namespace Adsysco\LaravelRegicareSoapClient\Models\Match;

use Adsysco\LaravelRegicareSoapClient\Models\Model;
use RuntimeException;
use Illuminate\Contracts\Support\Arrayable;

class MatchScope extends Model implements Arrayable
{
    const TYPE_MARKTPLAATS = 'marktplaats';
    const TYPE_PRAKTISCHE_HULP = 'praktische-hulp';

    const ROLE_VOLUNTEER = 'Vrijwilliger';
    const ROLE_CLIENT = 'Client';
    const ROLE_BOTH = 'Default';

    const VOLUNTEER_ROLE_FOR_TYPE = [
        self::TYPE_MARKTPLAATS => self::ROLE_BOTH,
        self::TYPE_PRAKTISCHE_HULP => self::ROLE_VOLUNTEER,
    ];

    const CLIENT_ROLE_FOR_TYPE = [
        self::TYPE_MARKTPLAATS => self::ROLE_BOTH,
        self::TYPE_PRAKTISCHE_HULP => self::ROLE_CLIENT,
    ];

    protected $type;

    protected $id;

    protected $role;

    protected $configuration;

    protected $appends = ['key'];

    public function __construct($type, $id, $role = null)
    {
        parent::__construct();
        if (!$this->isValidType($type)) {
            throw new RuntimeException('Invalid scope type ' . $type);
        }

        $role = $role ?? self::ROLE_BOTH;
        if (!$this->isValidRole($role)) {
            throw new RuntimeException('Invalid scope role ' . $role);
        }

        $this->setRawAttributes([
            'type' => $type,
            'id' => $id,
            'role' => $role
        ]);

        $this->type = $type;
        $this->id = $id;
        $this->role = $role;
    }

    /**
     * @return MatchConfiguration
     */
    public function getConfiguration()
    {
        if (!$this->configuration) {
            $this->configuration = MatchConfiguration::getForScope($this);
        }
        return $this->configuration;
    }

    public function getRouteParams()
    {
        return [
            'scopeType' => $this->type,
            'scopeId' => $this->id,
            'scopeRole' => $this->role
        ];
    }

    public function requiresLogin()
    {
        return $this->getConfiguration()->isEnabled('verplichten_login');
    }

    public function getTypeAttribute()
    {
        return $this->type;
    }

    public function getIdAttribute()
    {
        return $this->id;
    }

    public function getRoleAttribute()
    {
        return $this->role;
    }

    public function getKeyAttribute()
    {
        return "{$this->type}-{$this->id}-{$this->role}";
    }

    public function hasRoleVolunteer()
    {
        return $this->role === $this::ROLE_VOLUNTEER;
    }

    public function hasRoleClient()
    {
        return $this->role === $this::ROLE_CLIENT;
    }

    public function hasRoleBoth()
    {
        return $this->role === $this::ROLE_BOTH;
    }

    public function isMarktplaats()
    {
        return $this->type === self::TYPE_MARKTPLAATS;
    }

    public function isPraktischeHulp()
    {
        return $this->type === self::TYPE_PRAKTISCHE_HULP;
    }

    /**
     * @param $type
     * @return bool
     */
    private function isValidType($type): bool
    {
        return $type === self::TYPE_MARKTPLAATS || $type === self::TYPE_PRAKTISCHE_HULP;
    }

    /**
     * @param  string|null  $role
     * @return bool
     */
    private function isValidRole(?string $role): bool
    {
        return $role === self::ROLE_BOTH ||
            $role === self::ROLE_CLIENT ||
            $role === self::ROLE_VOLUNTEER;
    }
}
