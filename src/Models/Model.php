<?php
namespace Adsysco\LaravelRegicareSoapClient\Models;

use Carbon\Carbon;
use Adsysco\LaravelRegicareSoapClient\Contracts\Soap\Clients\SoapClient;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;
use Illuminate\Database\Eloquent\Concerns\HidesAttributes;
use Illuminate\Support\Arr;

/**
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
abstract class Model implements Arrayable, Jsonable
{
    use HasAttributes;
    use HidesAttributes;

    /**
     * @var SoapClient
     */
    protected $soap;

    /**
     * Boolean to indicate if the model already exists in the API.
     * @var bool
     */
    protected $exists = false;

    /**
     * Model constructor.
     * @param SoapClient $soap
     */
    protected function __construct(SoapClient $soap = null)
    {
        if (!$soap) {
            $soap = resolve(SoapClient::class);
        }
        $this->soap = $soap;
        $this->dateFormat = 'Y-m-d';
    }

    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    public function __set($key, $value)
    {
        return $this->setAttribute($key, $value);
    }

    /**
     * Determine if an attribute or relation exists on the model.
     *
     * @param  string  $key
     * @return bool
     */
    public function __isset($key)
    {
        return $this->offsetExists($key);
    }

    /**
     * Determine if the given attribute exists.
     *
     * @param  mixed  $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return !is_null($this->getAttribute($offset));
    }

    /**
     * Determine is the given key exists in the 'attributes' array.
     *
     * @param string $keyName
     * @return boolean
     */
    public function hasKey($keyName)
    {
        return array_key_exists($keyName, $this->attributes);
    }

    /**
     * Get an attribute from the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function getAttribute($key)
    {
        if (!$key) {
            return;
        }

        // If the attribute exists in the attribute array or has a "get" mutator we will
        // get the attribute's value. Otherwise, we will proceed as if the developers
        // are asking for a relationship's value. This covers both types of values.
        if ($this->hasKey($key) || $this->hasGetMutator($key)) {
            return $this->getAttributeValue($key);
        }

        // Here we will determine if the model base class itself contains this given key
        // since we do not want to treat any of those methods are relationships since
        // they are all intended as helper methods and none of these are relations.
        if (method_exists(self::class, $key)) {
            return;
        }

        return '';
    }

    /**
     * Get the attributes that should be converted to dates.
     *
     * @return array
     */
    public function getDates()
    {
        return [];
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Create a model that exists in the api from an array of attributes.
     *
     * @param array $attributes
     * @return static
     */
    public static function existingFromArray($attributes = [])
    {
        $model = new static();
        $model->exists = true;
        foreach (Arr::wrap($attributes) as $key => $value) {
            $model->setAttribute($key, $value);
        }
        return $model;
    }

    /**
     * Create a new model from an array of attributes.
     *
     * @param array $attributes
     * @return static
     */
    public static function newFromArray(array $attributes = [])
    {
        $model = new static();
        $model->exists = false;
        foreach ($attributes as $key => $value) {
            $model->setAttribute($key, $value);
        }
        return $model;
    }

    public function toArray()
    {
        return $this->attributesToArray();
    }

    /**
     * Convert the model instance to JSON.
     *
     * @param  int $options
     * @return string
     *
     * @throws \Illuminate\Database\Eloquent\JsonEncodingException
     */
    public function toJson($options = 0)
    {
        $json = json_encode($this->jsonSerialize(), $options);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw JsonEncodingException::forModel($this, json_last_error_msg());
        }

        return $json;
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    protected function formatDate($date, $fromFormat = null, $toFormat = 'd-m-Y')
    {
        if (empty($date)) {
            return '';
        }
        if (!$fromFormat) {
            $fromFormat = $this->dateFormat;
        }
        return Carbon::createFromFormat($fromFormat, $date)->format($toFormat);
    }

    protected function getFormattedDate($dateName)
    {
        if (!$this->hasKey($dateName)) {
            return null;
        }
        return $this->formatDate($this->attributes[$dateName]);
    }

    protected function formatDateLocalized($date, $fromFormat = null, $toFormat = '%d %B %Y')
    {
        setlocale(LC_TIME, 'Dutch', 'nl_NL', 'nl', 'nl_NL.UTF-8');
        if (!isset($date)) {
            return '';
        }
        if (!$fromFormat) {
            $fromFormat = $this->dateFormat;
        }
        return Carbon::createFromFormat($fromFormat, $date)->formatLocalized($toFormat);
    }
}
